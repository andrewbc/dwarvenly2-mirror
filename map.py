#!/usr/bin/env python

from cStringIO import StringIO
import struct
from array import array
import pylzma
import binascii

class Map(object):
	def __init__(self):
		self.tiles = None

	def decompress(self, filename):
		with open(filename+'.map_', 'rb') as in_f:
			with open(filename+'.map', 'wb') as out_f:
				tmp = in_f.read()
				print("read: "+tmp)
				if len(tmp) < 1:
					print("broke")
				else:
					decomp = pylzma.decompress(tmp)
					print("decomp: "+decomp)
					out_f.write(decomp)

	def load(self, filename):
		self.tiles = array('B')
		with open(filename+'.map', 'rb') as in_f:
			self.version, self.width, self.height, self.depth = struct.unpack_from('<IHHH', in_f.read(80))
			for i in range(0, self.width*self.height*self.depth, 4096):
				self.tiles.extend(struct.unpack_from('<4096B', in_f.read(4096)))