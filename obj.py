#!/usr/bin/env python

import os
import warnings

from pyglet import gl
from pyglet import image
from pyglet import graphics

class Material(object):
    diffuse = [.8, .8, .8]
    ambient = [.2, .2, .2]
    specular = [0., 0., 0.]
    emission = [0., 0., 0.]
    shininess = 0.
    opacity = 1.
    texture = None

    def __init__(self, name):
        self.name = name

class MaterialGroup(graphics.Group):
    def __init__(self, material, parent=None):
        self.material = material
        graphics.Group.__init__(self, parent=parent)

    def set_state(self, face=gl.GL_FRONT_AND_BACK):
        mat = self.material
        tex = mat.texture
        if tex:
            gl.glEnable(tex.target)
            gl.glBindTexture(tex.target, tex.id)
        else:
            gl.glDisable(gl.GL_TEXTURE_2D)

        gl.glMaterialfv(face, gl.GL_DIFFUSE, (gl.GLfloat * 4)(*(mat.diffuse + [mat.opacity])))
        gl.glMaterialfv(face, gl.GL_AMBIENT, (gl.GLfloat * 4)(*(mat.ambient + [mat.opacity])))
        gl.glMaterialfv(face, gl.GL_SPECULAR, (gl.GLfloat * 4)(*(mat.specular + [mat.opacity])))
        gl.glMaterialfv(face, gl.GL_EMISSION, (gl.GLfloat * 4)(*(mat.emission + [mat.opacity])))
        gl.glMaterialf(face, gl.GL_SHININESS, mat.shininess)

class MeshGroup(graphics.Group):
    def __init__(self, name, parent=None):
        graphics.Group.__init__(self, parent=parent)
        self.groups = []
    
    def set_state(self):
        gl.glPushClientAttrib(gl.GL_CLIENT_VERTEX_ARRAY_BIT)
        gl.glPushAttrib(gl.GL_CURRENT_BIT | gl.GL_ENABLE_BIT | gl.GL_LIGHTING_BIT)
        gl.glEnable(gl.GL_CULL_FACE)
        gl.glCullFace(gl.GL_BACK)
    
    def unset_state(self):
        gl.glPopAttrib()
        gl.glPopClientAttrib()

        

class OBJ:
    tex_groups = {}
    def __init__(self, filename, batch=None, group=None, file=None, path=None):
        self.materials = {}
        self.meshes = {}        # Name mapping
        self.mesh_list = []     # Also includes anonymous meshes

        if file is None:
            file = open(filename, 'r')

        if path is None:
            path = os.path.dirname(filename)
        self.path = path
        self.file = filename

        mesh = None
        group = None
        material = None

        vertices = [[0., 0., 0.]]
        normals = [[0., 0., 0.]]
        tex_coords = [[0., 0.]]
        
        self.objects = []

        for line in open(filename, "r"):
            if line.startswith('#'): 
                continue
            values = line.split()
            if not values: 
                continue

            if values[0] == 'v':
                vertices.append(map(float, values[1:4]))
            elif values[0] == 'vn':
                normals.append(map(float, values[1:4]))
            elif values[0] == 'vt':
                tex_coords.append(map(float, values[1:3]))
            elif values[0] == 'mtllib':
                self.load_material_library(values[1])
            elif values[0] in ('usemtl', 'usemat'):
                material = self.materials.get(values[1], None)
                if material is None:
                    warnings.warn('Unknown material: %s' % values[1])
                if mesh is not None:
                    group = MaterialGroup(material)
                    mesh.groups.append(group)
            elif values[0] == 'o':
                mesh = MeshGroup(values[1])
                self.meshes[mesh.name] = mesh
                self.mesh_list.append(mesh)
                group = None
            elif values[0] == 'f':
                if mesh is None:
                    mesh = MeshGroup('')
                    self.mesh_list.append(mesh)
                if material is None:
                    material = Material()
                if group is None:
                    group = MaterialGroup(material, parent=mesh)

                # For fan triangulation, remember first and latest vertices
                v1 = None
                vlast = None
                points = []
                index_count = 0
                meta_indices = {}
                indices = []
                vertices_= []
                normalces = []
                textureces = []
                for i, v in enumerate(values[1:]):
                    v_index, t_index, n_index = \
                        (map(int, [j or 0 for j in v.split('/')]) + [0, 0])[:3]
                    if v_index < 0:
                        v_index += len(vertices) - 1
                    if t_index < 0:
                        t_index += len(tex_coords) - 1
                    if n_index < 0:
                        n_index += len(normals) - 1
                    t, n, v = tex_coords[t_index], normals[n_index], vertices[v_index]
                    vertex = tuple(v+n+t)
                    try:
                        ind = meta_indices[vertex]
                    except KeyError:
                        ind = meta_indices[vertex] = index_count
                        index_count += 1
                        vertices_.extend(v)
                        normalces.extend(n)
                        textureces.extend(t)
                    indices.append(ind)
                    

                    if i >= 3:
                        # Triangulate
                        indices.extend((0, i-1))

                    if i == 0:
                        v1 = vertex
                    vlast = vertex
                    
                #print(indices, vertices_, normalces, textureces)
                self.objects.append({
                    'v': vertices_,
                    'i': indices,
                    'n': normalces,
                    't': textureces,
                    'g': group
                })
        self.load_material_library()
                #player_vl = batch.add_indexed(len(vertices_)/3, gl.GL_TRIANGLES, group,
                #            indices,
                #            ('v3f/static', vertices_),
                #            ('n3f/static', normalces),
                #            ('t2f/static', textureces))

    def add_at(self, pos, batch):
        x, y, z = map(float, pos)
        for obj in self.objects:
            #print obj['v']
            t = [x, y, z]*(len(obj['v'])/3)
            batch.add_indexed(len(obj['v'])/3, gl.GL_TRIANGLES, obj['g'],
                                    obj['i'],
                                    ('v3f/static', [float(t[i]+v) for i, v in enumerate(obj['v'])]),
                                    ('n3f/static', obj['n']),
                                    ('t2f/static', obj['t']))
                    
    def open_material_file(self, filename=None):
        '''Override for loading from archive/network etc.'''
        if filename is None:
            filename = self.file
            head, sep, tail = filename.rpartition('.')
            if head == '':
                return open(tail, 'r')
            else:
                return open(head+'.mtl', 'r')
        return open(os.path.join(self.path, filename), 'r')

    def load_material_library(self, filename=None):
        material = None
        file = self.open_material_file(filename)

        for line in file:
            if line.startswith('#'):
                continue
            values = line.split()
            if not values:
                continue

            if values[0] == 'newmtl':
                material = Material(values[1])
                self.materials[material.name] = material
            elif material is None:
                warnings.warn('Expected "newmtl" in %s' % filename)
                continue

            try:
                if values[0] == 'Kd':
                    material.diffuse = map(float, values[1:])
                elif values[0] == 'Ka':
                    material.ambient = map(float, values[1:])
                elif values[0] == 'Ks':
                    material.specular = map(float, values[1:])
                elif values[0] == 'Ke':
                    material.emissive = map(float, values[1:])
                elif values[0] == 'Ns':
                    material.shininess = float(values[1])
                elif values[0] == 'd':
                    material.opacity = float(values[1])
                elif values[0] == 'map_Kd':
                    try:
                        material.texture = image.load(os.path.join(self.path, values[1])).texture
                    except image.ImageException:
                        warnings.warn('Could not load texture %s' % values[1])
            except BaseException as e:
                warnings.warn('Parse error in %s\n\t%s' % (filename, e))