import socket
import json
import errno

print ("Setting up server...")
sock = socket.socket(socket.AF_INET)
sock.setblocking(False)
sock.bind((socket.gethostbyname(socket.getfqdn()), 44044))
sock.listen(5)
clients = {}

while True:
	try:
		conn = sock.accept()
	except socket.error as (errno, msg):
		if errno == 11:
			conn = False
		else:
			raise socket.error(errno, msg)

	if conn:
		cli_sock, address = conn
		cli_sock.setblocking(False)
		clients[address] = cli_sock
	for address, client in clients.items():
		from_buffer = ""
		
		try:
			s = client.recv(4096)
			from_buffer += s
			while len(s) >= 4096:
				s = client.recv(4096)
				from_buffer += s
		except socket.error as (errno, msg):
			if errno == 11:
				pass
		
		if from_buffer:
			print('got: '+from_buffer)
			client.send(from_buffer)
			from_buffer = ""
			#messages = from_buffer.split('\0')
			#for message in messages:
			#	message = json.loads(message)


