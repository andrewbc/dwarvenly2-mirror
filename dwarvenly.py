#!/usr/bin/env python
from __future__ import division
import pyglet
from pyglet.gl import *
from pyglet.window import key
import json
from os import path
import ctypes
import simplui as ui
from simplui import *

import obj
import shader
from net import Net
from map import Map
from tools import Camera, Perspective, frange

net = None

script_dir = path.dirname(__file__)
assets_dir = path.join(script_dir, 'assets')

pyglet.resource.path = ['assets', 'shaders', 'maps']
pyglet.resource.reindex()
# disable error checking for increased performance
pyglet.options['debug_gl'] = False

paths = {
    'dungeons': {
        'ceiling': path.join(assets_dir, 'ceiling', 'ceiling.obj'),
        'ceiling_column': path.join(assets_dir, 'ceiling_column', 'ceiling_with_column.obj'),
        'ceiling_small': path.join(assets_dir, 'small_ceilingpart', 'small_ceilingpart.obj'),
        'column': path.join(assets_dir, 'column', 'column.obj'),
        'corridor': path.join(assets_dir, 'corridor', 'corridor.obj'),
        'corridor_corner': path.join(assets_dir, 'corridor_cornerpiece', 'corridor_cornerpiece.obj'),
        'corridor_cross': path.join(assets_dir, 'corridor_crosspiece', 'corridor_crosspiece.obj'),
        'corridor_deadend': path.join(assets_dir, 'corridor_deadend', 'corridor_deadend.obj'),
        'corridor_door': path.join(assets_dir, 'corridor_door', 'corridor_door.obj'),
        'corridor_t': path.join(assets_dir, 'corridor_t-piece', 'corridor_t-piece.obj'),
        'ceiling_small': path.join(assets_dir, 'small_ceilingpart', 'small_ceilingpart.obj'),
        'cube': path.join(assets_dir, 'cube', 'cube.obj'),
        'stairs_a': path.join(assets_dir, 'stairs_a', 'stairs_a.obj'),
        'stairs_b': path.join(assets_dir, 'stairs_b', 'stairs_b.obj'),
        'wall': path.join(assets_dir, 'wall', 'wall.obj'),
        'wall_high': path.join(assets_dir, 'wall_high', 'wall_high.obj'),
        'wall_w_closed': path.join(assets_dir, 'wall_with_closed_windows', 'wall_with_closed_windows.obj'),
        'wall_w_opened': path.join(assets_dir, 'wall_with_open_windows', 'wall_with_open_windows.obj'),
        'wall_corner_in': path.join(assets_dir, 'wallcorner_inside', 'wallcorner_inside.obj'),
        'wall_corner_out': path.join(assets_dir, 'wallcorner_outside', 'wallcorner_outside.obj'),
        'wall_corner_out_double': path.join(assets_dir, 'wallcorner_outside_double', 'wallcorner_outside_double.obj'),
    }
}

meshes = {
    'dungeons': {
        'ceiling': obj.OBJ(paths['dungeons']['ceiling']),
        'ceiling_column': obj.OBJ(paths['dungeons']['ceiling_column']),
        'ceiling_small': obj.OBJ(paths['dungeons']['ceiling_small']),
        'column': obj.OBJ(paths['dungeons']['column']),
        'cube': obj.OBJ(paths['dungeons']['cube']),
        'corridor': obj.OBJ(paths['dungeons']['corridor']),
        'corridor_corner': obj.OBJ(paths['dungeons']['corridor_corner']),
        'corridor_cross': obj.OBJ(paths['dungeons']['corridor_cross']),
        'corridor_deadend': obj.OBJ(paths['dungeons']['corridor_deadend']),
        'corridor_door': obj.OBJ(paths['dungeons']['corridor_door']),
        'corridor_t': obj.OBJ(paths['dungeons']['corridor_t']),
        'stairs_a': obj.OBJ(paths['dungeons']['stairs_a']),
        'stairs_b': obj.OBJ(paths['dungeons']['stairs_b']),
        'wall': obj.OBJ(paths['dungeons']['wall']),
        'wall_high': obj.OBJ(paths['dungeons']['wall_high']),
        'wall_w_closed': obj.OBJ(paths['dungeons']['wall_w_closed']),
        'wall_w_opened': obj.OBJ(paths['dungeons']['wall_w_opened']),
        'wall_corner_in': obj.OBJ(paths['dungeons']['wall_corner_in']),
        'wall_corner_out': obj.OBJ(paths['dungeons']['wall_corner_out']),
        'wall_corner_out_double': obj.OBJ(paths['dungeons']['wall_corner_out_double']),
    }
}

background = pyglet.graphics.OrderedGroup(0)
foreground = pyglet.graphics.OrderedGroup(1)
ui_group = pyglet.graphics.OrderedGroup(2)
batch = pyglet.graphics.Batch()

resolution = (1280, 1024)
perspective = Perspective(60., 0., 100., resolution)
camera = Camera((0., 10., -10.), (0., 0., 0.), (0., 1., 0.))
window = pyglet.window.Window(*resolution, vsync=False)
keys = key.KeyStateHandler()
window.push_handlers(keys)

ui_themes = [ui.Theme("assets/uithemes/pywidget"),
             ui.Theme("assets/uithemes/macos")]
ui_theme = ui_themes[0]
ui_frame = ui.Frame(ui_theme, w=resolution[0], h=resolution[1])
window.push_handlers(ui_frame)

# these are some action callbacks, to be called by gui elements
def cycle_themes(button):
    # switch gui themes at runtime
    global ui_theme
    ui_theme = (ui_theme + 1) % len(ui_themes)
    ui_frame.theme = ui_themes[ui_theme]

def check_action(checkbox):
    print 'checkox', ('checked' if checkbox.value else 'unchecked')

def slider_action(slider):
    print 'slider moved to:', round(slider.value, 2)

def button_action(button):
    # when the button is clicked, we retrieve a named element from the gui
    element = ui_frame.get_element_by_name('misc_layout')
    # and add a new label to it
    element.add( ui.Label('This is another label...') )

def text_action(input):
    print 'text entered:', input.text

main_menu = ui.Dialogue("Main Menu", x=500, y=550, content=
                        ui.FlowLayout(w=350, children=[
                            ui.Label("Hello, world!"),
                            ui.Button("New Game"),
                            ui.Button("Load Game"),
                            ui.Button("Multiplayer"),
                        ])
                        )
ui_frame.add(main_menu)

dialogue = ui.Dialogue('Inspector', x=200, y=500, content=
# add a vertical layout to hold the window contents
ui.VLayout(autosizex=True, hpadding=0, children=[
    # now add some folding boxes
    ui.FoldingBox('stats', content=
                  # each box needs a content layout
                  ui.VLayout(children=[
                      # add a text label, note that this element is named...
                      ui.Label('0.0 fps', name='fps_label'),
                      # and this element is not named
                      ui.Label('10,000 triangles')
                  ])
                  ),
    ui.FoldingBox('settings', content=
                  ui.VLayout(children=[
                      # a clickable button to change the theme
                      ui.Button('Change GUI Theme', action=cycle_themes),
                      # a slider, with label
                      ui.HLayout(children=[
                          ui.Label('Detail:', halign='right'),
                          ui.Slider(w=100, min=0.0, max=4.0, action=slider_action),
                          ]),
                      ui.HLayout(children=[
                          ui.Label('Intensity:', halign='right'),
                          ui.Slider(w=100, min=0.0, max=4.0, action=slider_action),
                          ]),
                      # a checkbox, note the action function is provided directly
                      ui.Checkbox('Show wireframe', h=100, action=check_action),
                      # a text input field, with label
                      ui.HLayout(children=[
                          ui.Label('Name:', hexpand=False),
                          ui.TextInput(text='edit me', action=text_action)
                      ])
                  ])
                  ),
    ui.FoldingBox('misc', content=
                  # We need to name this layout, because we used it in the callback above
                  ui.VLayout(name='misc_layout', children=[
                      # a random label
                      ui.Label('Hello, World!'),
                      # and a clickable button
                      ui.Button('Click me!', action=button_action)
                  ])
                  )
])
)
# add the dialogue to the frame
ui_frame.add( dialogue )


label = pyglet.text.Label('Hello, world!', font_name="Times New Roman",
                          font_size=36, x=window.width//2, y=window.height//2,
                          anchor_x="center", anchor_y="center")

player_vl = batch.add_indexed(4, pyglet.gl.GL_TRIANGLES, foreground,
                              (0, 1, 2, 2, 1, 3),
                              ('v3f', (0.0, 0.0, 1.0,  0.0, 1.0, 1.0,  1.0, 0.0, 1.0,  1.0, 1.0, 1.0)),
                              ('c3B', (255,   0, 255,    0, 255,   0,  255,   0,   0,    0,   0, 255))
)

fps_display = pyglet.clock.ClockDisplay()

r = 0

physics_accum = 0.0
physics_step = 0.01

def update(dt=0.0):
    global physics_accum
    global physics_step
    physics_accum += dt
    physics_accum_ = physics_accum
    physics_step_ = physics_step
#    l_entities = self.physicals

    while physics_accum_ >= physics_step_:
        #for entity in entities_:
        #    entity.update(physics_step_)
        camera.update(physics_step_, keys)
        physics_accum_ -= physics_step_
    interpolation_rate = physics_accum_ / physics_step_
    camera.interp = interpolation_rate
    physics_accum = physics_accum_

class ExitHandler(object):
    running = True
    def on_close(self):
        self.running = False
    def on_keypress(self, symbol, modifiers):
        if symbol == pyglet.window.key.K_ESCAPE:
            self.running = False
        return EVENT_UNHANDLED
exit_handler = ExitHandler()
window.push_handlers(exit_handler)

@window.event
def on_draw():
    window.clear()
    perspective.apply()
    glLoadIdentity()
    camera.draw()
    batch.draw()
    glViewport(0, 0, resolution[0], resolution[1]) 
    glMatrixMode(GL_PROJECTION) 
    glLoadIdentity()
    glOrtho(0, resolution[0], 0, resolution[1], -1, 1) 
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    fps_display.draw()
    ui_frame.draw()


@window.event
def on_resize(width, height):
    if height < 1:
        height = 1
    if width < 1:
        width = 1
    perspective.set_size(width, height)
    perspective.apply()

    fourfv = ctypes.c_float * 4
    c_float_p = ctypes.POINTER(ctypes.c_float)
    glLightfv(GL_LIGHT0, GL_POSITION, ctypes.cast(fourfv(100, 200, 100, 0), c_float_p))
    glLightfv(GL_LIGHT0, GL_AMBIENT, ctypes.cast(fourfv(0.5, 0.5, 0.5, 1.0), c_float_p))
    glLightfv(GL_LIGHT0, GL_DIFFUSE, ctypes.cast(fourfv(0.6, 0.6, 0.6, 1.0), c_float_p))
    glEnable(GL_LIGHT0)
    glEnable(GL_LIGHTING)
    glEnable(GL_COLOR_MATERIAL)
    glEnable(GL_DEPTH_TEST)
    glShadeModel(GL_SMOOTH)

    glPolygonMode(GL_FRONT, GL_FILL)
    glDepthFunc(GL_LESS)
    glEnable(GL_CULL_FACE)
    glCullFace(GL_BACK)
    return pyglet.event.EVENT_HANDLED

# schedule a update function to be called less often
def update_stats(dt):
    # if the dialogue is onscreen, update the fps counter
    if dialogue.parent != None:
        fps = pyglet.clock.get_fps()
        # retrieve the named element from the gui
        element = ui_frame.get_element_by_name('fps_label')
        # and change the text, to display the current fps
        element.text = '%.1f fps' % (fps)
pyglet.clock.schedule_interval(update_stats, 0.5)

if __name__ == '__main__':

    def init(data):
        for z in range(-10, 10, 2):
            for x in range(-10, 10, 2):
                meshes['dungeons']['cube'].add_at((x, -1, z), batch=batch)

    net = Net('173.255.198.102', 44044)
    net.send(type='init', id='bacon', re=init)
    #map = Map()
    assert pyglet.gl.gl_info.have_extension('GL_ARB_draw_instanced')
    assert pyglet.gl.gl_info.have_extension('GL_ARB_instanced_arrays')
    assert pyglet.gl.gl_info.have_version(3, 1, 0)
    #print(pyglet.gl.gl_info.get_version())
    #glDrawElementsInstanced()

    #print("@@@"+pyglet.resource.file('instancing.vert').read())
    #v_s = shader.ShaderProgram('instancing')
    #v_s.bind()

    #f_s = shader.ShaderProgram('red', 'desaturate')
    #f_s.bind()
    #f_s.uniformf('tint', 1.0, 0.0, 0.0, 1.0)
    #f_s.uniformf('strength', 0.7)


    pyglet.clock.schedule(update)
    pyglet.app.run()