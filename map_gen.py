#!/usr/bin/env python
from __future__ import division
from __future__ import print_function
import random
import os.path
import math
import json
import binascii
from cStringIO import StringIO
import pylzma
from cNoise import perlin3

tile_spectrum = [
    'space',

    # grounds
    'sand:red',
    'sand:orange',
    'sand:black',
    'sand:yellow',
    'sand:white',
    'dirt:red',
    'dirt:orange',
    'dirt:black',
    'dirt:yellow',
    'dirt:brown',
    'silt:red',
    'silt:orange',
    'silt:black',
    'silt:yellow',
    'silt:brown',

    # manmades
    'floor:terrible',
    'floor:poor',
    'floor:passable',
    'floor:good',
    'floor:great',
    'floor:perfect',
    'floor:godlike',
    'wall:terrible',
    'wall:poor',
    'wall:passable',
    'wall:good',
    'wall:great',
    'wall:perfect',
    'wall:godlike',

    # igneous rocks
    # \----> plutonic (forms in crust from magma)
    'granite',
    'diorite',
    'gabbro',
    'peridotite',
    # \----> volcanic (forms in atmosphere from lava)
    'pumice',
    'basalt',
    'andesite',
    'rhyolite',
    'obsidian',

    # sedimentary rocks
    # \----> mud rocks
    'mudstone',
    'shale',
    'siltstone',
    # \----> sandstones
    'sandstone',
    # \----> carbonate
    'limestone',
    'dolostone',

    # metamorphic rocks
    'gneiss',
    'slate',
    'marble',
    'schist',
    'quartzite'

    # ores
    'platinum',
    'gold',
    'silver',
    'copper',
    'tin',
    'aluminium',
    'lead',
    'iron',
    'coal',
    'uranium',
    'adamantite',

    # gems
    'pyrite',
    'quartz',
    'malachite',
    'jasper',
    'amethyst',
    'lapis lazuli',
    'ruby',
    'moss agate',
    'lace agate',
    'rose quartz',
    'hematite',
    'turqoise',
    'emerald',
    'sapphire',
    'aquamarine',
    'golden beryl',
    'red beryl',
    'morganite',
    'amber',
    'opal',
    'diamond',
    'pearl',
    'axinite',
    'cassiterite',
    'clinohumite',
    'topaz',
]

def closest(x, y, points):
    closest = None
    distance = None
    for i, (x2, y2) in enumerate(points):
        d = (x2-x)**2+(y2-y)**2
        if d == 0:
            return i, 0
        if d < distance or not distance:
            closest = i
            distance = d
    return closest, distance

def make_control_points(n, start=0, stop=1024, step=1):
    control_points = []
    for i in range(n):
        x = random.randrange(start, stop, step)
        y = random.randrange(start, stop, step)
        control_points.append((x, y))
    return control_points

def gen_to(file, width=256, height=256, depth=256, ground_base=240, steepness=10):
    # ocean is the default
    print("Generating map terrain")
    if width > 65536:
        width = 65536
    if height > 65536:
        height = 65536
    if depth > 65536:
        depth = 65536
    if width < 1:
        width = 1
    if height < 1:
        height = 1
    if depth < 1:
        depth = 1
    map = []
    min = 0
    max = 0
    low_w = width//4
    low_h = height//4
    noises = [[0.5+perlin3(x/low_w, y/low_h, 0.0) for y in range(height)] for x in range(width)]

    print("Writing uncompressed map")
    with open(file+'.map', 'wb') as f:
        s = binascii.unhexlify("00000000"+"%0.4X" % (width-1,)+"%0.4X" % (height-1,)+"%0.4X" % (depth-1,))
        z = 0
        for i, tile in enumerate(range(width*height*depth)):
            z_ = z
            z = i // 65536
            j = i  % 65536
            y = j // 256
            x = j  % 256
            #print('%s, %s, %s' % (x, y, z))
            offset = noises[x][y]
            d = 256 - int(ground_base+(offset*steepness))
            if z < d:
                # sky
                s += binascii.unhexlify('00')
            else:
                s += binascii.unhexlify('01')
            if z_ != z:
                f.write(s)
                s = ''

    print("Writing compressed map")
    with open(file+'.map', 'rb') as in_f:
        with open(file+'.map_', 'wb') as out_f:
            fp = pylzma.compressfile(in_f, eos=1, dictionary=20, fastBytes=255)
            while True:
                tmp = fp.read(4096)
                if not tmp: break;
                out_f.write(tmp)

    print("Done!")

if __name__ == '__main__':
    file = raw_input("name of file to output? ")
    is_ok = False
    while os.path.exists(file) and not is_ok:
        is_ok = raw_input("that exists already, is this okay? ")
        if is_ok.lower() not in ("y", "yes", "yup", "affirmative"):
            file = raw_input("name of file to output? ")

    seed = raw_input("random seed? ")
    if seed.lower() in ("", "no"):
        seed = None
    random.seed(seed)
    gen_to(file)