#!/usr/bin/env python
# encoding: utf-8
"""
tools.py

Created by Andrew on 2011-08-03.
Copyright (c) 2011 __MyCompanyName__. All rights reserved.
"""
from pyglet import gl
from pyglet.window import key
import math

from actors import Actor

class Perspective:
    def __init__(self, FOVY, zNear, zFar, resolution):
        self.FOVY = FOVY
        if zNear == 0:
            zNear += 0.001
        self.zNear = zNear
        self.zFar = zFar
        self.set_size(*resolution)
        print(self.FOVY, self.aspectRatio, self.zNear, self.zFar)

    def set_size(self, width, height):
        self.width, self.height = int(width), int(height)
        if self.width <= 0.:
            self.width = 1.
        if self.height <= 0.:
            self.height = 1.
        self.aspectRatio = float(self.width) / float(self.height)

    def apply(self):
        gl.glViewport(0, 0, int(self.width), int(self.height))
        gl.glMatrixMode(gl.GL_PROJECTION)
        gl.glLoadIdentity()
        #print(self.FOVY, self.aspectRatio, self.zNear, self.zFar)
        gl.gluPerspective(self.FOVY, self.aspectRatio, self.zNear, self.zFar)
        gl.glMatrixMode(gl.GL_MODELVIEW)

    def draw(self):
        gl.gluPerspective(self.FOVY, self.aspectRatio, self.zNear, self.zFar)

class Camera(Actor):
    def __init__(self, position, focus, up):
        x, y, z  = position
        Actor.__init__(self, x, y, z)
        
        self.fx, self.fy, self.fz = self.fx_, self.fy_, self.fz_ = focus
        self.ux, self.uy, self.uz = up
        
        self._defaultPosition = position
        self._defaultFocus = focus
        self._defaultUp = up
        
        self.moveSpeed = 0.0003
        self.interp = 0.0

    def update(self, dt, KeyStatus):
        offset = dt / self.moveSpeed
        off_x, off_y, off_z = 0., 0., 0.
        rot_x, rot_y, rot_z = 0., 0., 0.
        
        if KeyStatus[key.LEFT] and not KeyStatus[key.RIGHT]:
            off_x, off_y, off_z = self.left
            if not KeyStatus[key.LSHIFT] and not KeyStatus[key.RSHIFT]:
                rot_x, rot_y, rot_z = self.left
        elif KeyStatus[key.RIGHT] and not KeyStatus[key.LEFT]:
            off_x, off_y, off_z = self.right
            if not KeyStatus[key.LSHIFT] and not KeyStatus[key.RSHIFT]:
                rot_x, rot_y, rot_z = self.right
        if KeyStatus[key.DOWN] and not KeyStatus[key.UP]:
            if KeyStatus[key.LCTRL] or KeyStatus[key.RCTRL]:
                off_x, off_y, off_z = self.down
                if not KeyStatus[key.LSHIFT] and not KeyStatus[key.RSHIFT]:
                    rot_x, rot_y, rot_z = self.down
            else:
                off_x, off_y, off_z = self.backward
                rot_x, rot_y, rot_z = self.backward
        elif KeyStatus[key.UP] and not KeyStatus[key.DOWN]:
            if KeyStatus[key.LCTRL] or KeyStatus[key.RCTRL]:
                off_x, off_y, off_z = self.up
                if not KeyStatus[key.LSHIFT] and not KeyStatus[key.RSHIFT]:
                    rot_x, rot_y, rot_z = self.up
            else:
                off_x, off_y, off_z = self.forward
                rot_x, rot_y, rot_z = self.forward
                
        self.x_ = self.x
        self.y_ = self.y
        self.z_ = self.z
        self.fx_ = self.fx
        self.fy_ = self.fy
        self.fz_ = self.fz
        self.x  += off_x / offset
        self.y  += off_y / offset
        self.z  += off_z / offset
        self.fx += rot_x / offset
        self.fy += rot_y / offset
        self.fz += rot_z / offset

    def draw(self):
        interp  = self.interp
        interp_ = 1-interp 
        gl.glLoadIdentity()
        gl.gluLookAt(self.x*interp + self.x_*interp_,
                     self.y*interp + self.y_*interp_,
                     self.z*interp + self.z_*interp_,
                     self.fx*interp + self.fx_*interp_,
                     self.fy*interp + self.fy_*interp_,
                     self.fz*interp + self.fz_*interp_,
                     self.ux,
                     self.uy,
                     self.uz)

    def reset(self):
        self.x,  self.y,  self.z  = self._defaultPosition
        self.fx, self.fy, self.fz = self._defaultFocus
        self.ux, self.uy, self.uz = self._defaultUp
        
    @property
    def up(self):
        return v_normalize(self.ux, self.uy, self.uz)

    @property
    def forward(self):
        return v_normalize(*v_sub(self.fx, self.fy, self.fz, self.x, self.y, self.z))
        
    @property
    def backward(self):
        return v_negate(*self.forward)

    @property
    def right(self):
        x, y, z = self.forward
        x_, y_, z_ = self.up
        return v_normalize(*v_cross(x, y, z, x_, y_, z_))

    @property
    def left(self):
        x, y, z = self.forward
        x_, y_, z_ = self.up
        return v_normalize(*v_cross(x_, y_, z_, x, y, z))

    @property
    def down(self):
        return v_negate(*self.up)

def frange(start, stop, step):
    while start <= stop:
        yield start
        start += step

def v_dist(x1, y1, z1, x2, y2, z2):
    x_, y_, z_ = v_sub(x2, y2, z2, x1, y1, z1)
    return v_sqrt(*v_sum(*v_pow(x_, y_, z_, 2, 2, 2)))

def v_dot(x1, y1, z1, x2, y2, z2):
    return v_sum(*v_mul(x1, y1, z1, x2, y2, z2))

def v_normalize(x, y, z):
    l = math.sqrt(v_dot(x, y, z, x, y, z))
    return v_div(x, y, z, l, l, l)

def v_negate(x, y, z):
    return -x, -y, -z

def v_sum(x, y, z):
    return x+y+z

def v_add(x1, y1, z1, x2, y2, z2):
    return x1+x2, y1+y2, z1+z2

def v_sub(x1, y1, z1, x2, y2, z2):
    return x1-x2, y1-y2, z1-z2

def v_pow(x1, y1, z1, x2, y2, z2):
    return x1**x2, y1**y2, z1**z2
    
def v_sqrt(x, y, z):
    return math.sqrt(x), math.sqrt(y), math.sqrt(z)
    
def v_mul(x1, y1, z1, x2, y2, z2):
    return x1*x2, y1*y2, z1*z2

def v_div(x1, y1, z1, x2, y2, z2):
    return x1/x2, y1/y2, z1/z2
    
def v_cross(x1, y1, z1, x2, y2, z2):
    return y1*z2-z1*y2, z1*x2-x1*z2, x1*y2-y1*x2
    