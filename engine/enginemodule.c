#include <stdio.h>
#include <string.h>
#include <Python.h>
#include <GL3/gl3w.h>
#include <GL/glfw.h>

static int have_inited = 0;
static int w_width = 1;
static int w_height = 1;

typedef struct perspective {
    float fovy;
    float aspect;
    float znear;
    float zfar;
} perspective_t;

static perspective_t persp = {60.0, 1.0, 0.1, 100.0};

typedef struct camera {
    double x;
    double y;
    double z;
    double fx;
    double fy;
    double fz;
    double ux;
    double uy;
    double uz;

    double x_prev;
    double y_prev;
    double z_prev;

    double fx_prev;
    double fy_prev;
    double fz_prev;
} camera_t;

static camera_t cam = {0.0, 10.0, -10.0,
                       0.0,  0.0,   0.0,
                       0.0,  1.0,   0.0,
                       0.0, 10.0, -10.0,
                       0.0,  0.0,   0.0};


static int running = 1;

static PyObject *InitError;
static PyObject *ShaderError;

typedef struct shader {
	GLsizei buf_num;
	GLchar **buf;
	GLint *buf_len;
} shader_t;

#define BUF_SIZE 1024

void read_file(FILE *f, shader_t *s) {
	unsigned buff_len = 0;

	GLchar *buff;
	buff = malloc(sizeof(GLchar)*BUF_SIZE);

	unsigned buffs_i = 0;

	GLchar **buffs;
    buffs = malloc(sizeof(GLchar*));

	GLint *buffs_lens;
	buffs_lens = malloc(sizeof(GLint));

	void *temp;

    while (1) {
        buff_len += fread(buff, sizeof(GLchar), BUF_SIZE-buff_len, f);

        if (feof(f)) {
			buffs[buffs_i] = buff;
			buffs_lens[buffs_i] = buff_len;
			s->buf_num = buffs_i+1;
			s->buf = buffs;
			s->buf_len = buffs_lens;
            return;
        }

        if (ferror(f)) {
            free(s);
            s = NULL;
            return;
        }

        // Check for moving on to next buffer line
        if (buff_len >= BUF_SIZE) {
			// Persistence first
			buffs[buffs_i] = buff;
			buffs_lens[buffs_i] = buff_len;
			buffs_i += 1;

			// New line
			buff = malloc(sizeof(GLchar)*BUF_SIZE);
			buff_len = 0;

            // Add another line to persisters
            temp = realloc(buffs, sizeof(GLchar*)*(buffs_i+1));
            if (temp == NULL) {
				free(s);
				s = NULL;
                return;
            }
			buffs = temp;

			// Add another line len to persisters
			temp = realloc(buffs_lens, sizeof(GLint)*(buffs_i+1));
			if (temp == NULL) {
				free(s);
				s = NULL;
				return;
			}
			buffs_lens = temp;
        }
    }
}


void GLFWCALL win_on_resize(int width, int height) {
	if (width < 1) { width = 1; }
	if (height < 1) { height = 1; }
    w_width = width;
    w_height = height;
    persp.aspect = (float)width / (float)height;
	return;
}

static PyObject *win_open(PyObject *self, PyObject *args, PyObject *kwargs) {
    int width = 0; // GLFW sets to 640 if 0
    int height = 0; // GLFW sets to 480 if 0
    int redbits = 0;
    int greenbits = 0;
    int bluebits = 0;
    int alphabits = 0;
    int depthbits = 0;
    int stencilbits = 0;
    int windowed = GLFW_WINDOW;
    int success = 0;

    static char *kwlist[] = {"width", "height", "redbits", "greenbits", "bluebits", "alphabits", "depthbits", "stencilbits", "windowed", NULL};

    if (have_inited == 0) {
        success = glfwInit();
        if (success != GL_TRUE) {
            PyErr_SetString(InitError, "glfwInit() failed");
            return NULL;
        }
        have_inited = 1;
    }

    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "|iiiiiiiii", kwlist,
                                     &width, &height, &redbits, &greenbits,
                                     &bluebits, &alphabits, &depthbits,
                                     &stencilbits, &windowed)) {
            return NULL;
    }

    if (windowed != GLFW_WINDOW) {
        windowed = GLFW_FULLSCREEN;
    }

    success = glfwOpenWindow(width, height, redbits, greenbits, bluebits, alphabits, depthbits, stencilbits, windowed);
    if (success != GL_TRUE) {
        PyErr_SetString(InitError, "glfwOpenWindow() failed");
		glfwTerminate();
        return NULL;
    }

    glfwSetWindowSizeCallback( &win_on_resize );

    if (gl3wInit() != 0) {
        PyErr_SetString(InitError, "gl3wInit() failed");
        return NULL;
    }

    if (!gl3wIsSupported(3, 3)) {
        PyErr_SetString(InitError, "OpenGL 3.3 not supported on this device");
        return NULL;
    }

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *win_close(PyObject *self, PyObject *args) {
    glfwCloseWindow();

    Py_INCREF(Py_None);
    return Py_None;
}

static void on_exit(void) {
    running = 0;
}

static PyObject *run(PyObject *self, PyObject *args) {
    double curr = glfwGetTime();
    double prev = curr;
    double dt = 0.0;
    double phys_accum = 0.0;
    double phys_step = 0.01;
    double nextpolation = 0.0;
    double prevpolation = 0.0;

    Py_AtExit(on_exit);

    while(running) {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        prev = curr;
        curr = glfwGetTime();
        dt = curr - prev;

        phys_accum += dt;
        while(phys_accum >= phys_step) {
            // PHYSICS
            phys_accum -= phys_step;
        }

        nextpolation = (phys_accum / phys_step);
        prevpolation = 1 - nextpolation;

        /* glLoadIdentity(); */
        /*
		gluLookAt(cam.x*nextpolation + cam.x_prev*prevpolation,
                  cam.y*nextpolation + cam.y_prev*prevpolation,
                  cam.x*nextpolation + cam.z_prev*prevpolation,
                  cam.fx*nextpolation + cam.fx_prev*prevpolation,
                  cam.fy*nextpolation + cam.fy_prev*prevpolation,
                  cam.fz*nextpolation + cam.fz_prev*prevpolation,
                  cam.ux,
                  cam.uy,
                  cam.uz);
		*/

        /* NORMAL shit with a PERSPECTIVE projection */

        /* UI shit with an ORTHO projection */
        /*
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, w_width, 0, w_height, -1, 1);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        */

        glfwSwapBuffers();
    }

    glfwCloseWindow();
    glfwTerminate();

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *mk_shader(PyObject *self, PyObject *args, PyObject *kws) {
    GLuint shader_id;
    unsigned int type=0;
    const char *filename;
    const char *shader_type=NULL;

    static char *kwslist[] = {"filename", "type", NULL};

    if (!PyArg_ParseTupleAndKeywords(args, kws, "s|z:shader", kwslist,
									 &filename, &shader_type)) {
        return NULL;
    }

    if (shader_type != NULL && strlen(shader_type) > 0) {
        // User attempting to explicitly set the shader type.
		switch(shader_type[0]) {
			case 'v':
			case 'V':
				type = 1;
				break;
			case 'f':
			case 'F':
				type = 2;
				break;
			case 'g':
			case 'G':
				type = 3;
				break;
			default:
				PyErr_Format(ShaderError, "Unrecognized explicitly set shader type '%s'. Must start with case insensitive 'v' 'f' or 'g' for Vertex, Fragment, and Geometry shaders respectively.", shader_type);
				return NULL;
		}
    } else {
        // Attempt to determine shader type from filename extension
		char *ext = NULL;
		ext = strrchr(filename, '.');
		if (ext == NULL) {
			PyErr_Format(ShaderError, "Unable to determine shader type from filename '%s', no extension found.", filename);
			return NULL;
		}

		switch(ext[1]) { // Check first char of extension.
			case 'v':
            case 'V':
                type = 1;
                break;
            case 'f':
            case 'F':
                type = 2;
                break;
            case 'g':
            case 'G':
                type = 3;
                break;
            default:
                PyErr_Format(ShaderError, "Unrecognized shader extension '%s'. Either use the type keyword argument or extension must start with case insensitive 'v' 'f' or 'g' for Vertex, Fragment, and Geometry shaders respectively.", ext);
                return NULL;
		}
    }

    switch(type) {
        case 1:
            shader_id = glCreateShader(GL_VERTEX_SHADER);
            break;
        case 2:
            shader_id = glCreateShader(GL_FRAGMENT_SHADER);
            break;
        case 3:
            shader_id = glCreateShader(GL_GEOMETRY_SHADER);
            break;
        default:
            PyErr_SetString(ShaderError, "Unknown shader_id type. Something went catastrophically wrong with shader type detection.");
            return NULL;
    }

	FILE *f = fopen(filename, "r");
    if (f == NULL) {
        PyErr_Format(PyExc_IOError, "Failed to open shader file: %s", filename);
        return NULL;
    }

	shader_t s;
   	read_file(f, &s);
    fclose(f);

    if (&s == NULL) {
        PyErr_Format(PyExc_IOError, "Failed to read shader source from file: %s", filename);
        return NULL;
    }

    if (s.buf_num == 1 && s.buf_len[0] < 1) {
        PyErr_Format(PyExc_IOError, "Empty shader source file: %s", filename);
        return NULL;
    }

    glShaderSource(shader_id, s.buf_num, (const GLchar **)s.buf, s.buf_len);
    glCompileShader(shader_id);

	GLint status = GL_FALSE;
	glGetShaderiv(shader_id, GL_COMPILE_STATUS, &status);

	// Compile fail check
	if (status != GL_TRUE) {
		GLint log_len = 0;
		glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &log_len);
		// No log?
		if (log_len < 1) {
			PyErr_Format(ShaderError, "Shader compile error: file '%s', but no information log available. Blame OpenGL.", filename);
			return NULL;
		} else {
			GLchar *log;
			log = malloc(log_len * sizeof(GLchar));
			// malloc failed?
			if (log == NULL) {
				PyErr_Format(ShaderError, "Shader compile error: file '%s', Additionally, failed to obtain space for compile log.", filename);
				return NULL;
			}

			glGetShaderInfoLog(shader_id, log_len, NULL, log);
			PyErr_Format(ShaderError, "Shader compile error: file '%s', log: %s", filename, log);
			free(log);
			return NULL;
		}
	}

    return Py_BuildValue("I", shader_id);
}

static PyObject *rm_shader(PyObject *self, PyObject *args) {
	GLuint shader_id;
	if (!PyArg_ParseTuple(args, "I", &shader_id)) {
		return NULL;
	}

	glDeleteShader(shader_id);

	Py_INCREF(Py_None);
	return Py_None;
}

static PyObject *mk_program(PyObject *self, PyObject *args) {
	GLuint program_id;
	Py_ssize_t arg_num = PyTuple_Size(args);
	GLuint *shader_ids = malloc(arg_num * sizeof(GLuint));

	// Make sure we have at least one arg
	if (arg_num < 1) {
		PyErr_SetString(ShaderError, "No shader ids specified for shader program creation. Need at least 1.");
		return NULL;
	}

	// Gather up all the shader_ids passed in
	PyObject *curr;
	PyObject *temp;
	Py_ssize_t i;
	for (i=0; i<arg_num; i++) {
		curr = PyTuple_GetItem(args, i);
		if (curr == NULL) {
			free(shader_ids);
			return NULL;
		}

		if (PyNumber_Check(curr) != 1) {
			PyErr_SetString(PyExc_TypeError, "Non-numeric argument for shader id.");
			free(shader_ids);
			return NULL;
		}

		temp = PyNumber_Long(curr);
		shader_ids[i] = PyLong_AsUnsignedLong(temp);
		Py_DECREF(temp);
	}

	program_id = glCreateProgram();
	if (program_id == 0) {
		PyErr_SetString(ShaderError, "Creating the shader program failed, glCreateProgram returned 0.");
		free(shader_ids);
		return NULL;
	}

	for (i=0; i<arg_num; i++) {
		glAttachShader(program_id, shader_ids[i]);
	}

	glLinkProgram(program_id);

	// Check for linking success
	GLint success = GL_FALSE;
	glGetProgramiv(program_id, GL_LINK_STATUS, &success);
	if (success != GL_TRUE) {
		// Get info log length
		GLint log_len = 0;
		glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &log_len);
		if (log_len < 1) {
			PyErr_Format(ShaderError, "Linking program %u failed. Additionally, fetching info log for the program failed.", program_id);
			free(shader_ids);
			return NULL;
		}

		// Return info log as error
		GLchar *log = malloc(sizeof(GLchar)*log_len);
		if (log == NULL) {
			PyErr_Format(ShaderError, "Linking program %u failed. Additionally, fetching memory for the info log also failed.", program_id);
			free(shader_ids);
			return NULL;
		}
		glGetProgramInfoLog(program_id, log_len, NULL, log);
		PyErr_Format(ShaderError, "Linking program %u failed. Info: %s", program_id, log);
		free(shader_ids);
		free(log);
		return NULL;
	}

	// Validate program works
	success = GL_FALSE;
	glValidateProgram(program_id);
	glGetProgramiv(program_id, GL_VALIDATE_STATUS, &success);
	if (success != GL_TRUE) {
        // Get info log length
        GLint log_len = 0;
        glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &log_len);
        if (log_len < 1) {
            PyErr_Format(ShaderError, "Validating program %u failed. Additionally, fetching info log for the program failed.", program_id);
            free(shader_ids);
            return NULL;
        }

        // Return info log as error
        GLchar *log = malloc(sizeof(GLchar)*log_len);
        if (log == NULL) {
            PyErr_Format(ShaderError, "Validating program %u failed. Additionally, fetching memory for the info log also failed.", program_id);
            free(shader_ids);
            return NULL;
        }
        glGetProgramInfoLog(program_id, log_len, NULL, log);
        PyErr_Format(ShaderError, "Validating program %u failed. Info: %s", program_id, log);
        free(shader_ids);
        free(log);
        return NULL;
	}

	// \o/
	return Py_BuildValue("I", program_id);
}

static PyObject *rm_program(PyObject *self, PyObject *args) {
	GLuint program_id;
    if (!PyArg_ParseTuple(args, "I", &program_id)) {
        return NULL;
    }

    glDeleteProgram(program_id);

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *use_program(PyObject *self, PyObject *args) {
	GLuint program_id;
	if (!PyArg_ParseTuple(args, "I", &program_id)) {
		return NULL;
	}

	glUseProgram(program_id);

	Py_INCREF(Py_None);
	return Py_None;
}

static PyMethodDef py_methods[] = {
    {"win_open", (PyCFunction)win_open, METH_KEYWORDS, "Open GLFW Window."},
    {"win_close", (PyCFunction)win_close, METH_NOARGS, "Close GLFW Window. (Does not need to be explicitly called)"},
    {"run", (PyCFunction)run, METH_NOARGS, "Run game loop."},
    {"mk_shader", (PyCFunction)mk_shader, METH_KEYWORDS, "Compile shader given filename, returns shader id."},
    {"rm_shader", (PyCFunction)rm_shader, METH_VARARGS, "Mark shader for deletion, given a shader id."},
	{"mk_program", (PyCFunction)mk_program, METH_VARARGS, "Link given shader_id set into a program, returns program id."},
	{"rm_program", (PyCFunction)rm_program, METH_VARARGS, "Mark program for deletion, given a program id."},
	{"use_program", (PyCFunction)use_program, METH_VARARGS, "Enable the program object."},
	{NULL, NULL, 0, NULL}
};

PyMODINIT_FUNC initengine(void) {
    PyObject *m;
    m = Py_InitModule("engine", py_methods);
    if (m == NULL) {
        return; // QQ
    }

    // initialize and make exceptions into members of the module
    InitError = PyErr_NewException("engine.InitError", NULL, NULL);
    Py_INCREF(InitError);
    PyModule_AddObject(m, "InitError", InitError);

    ShaderError = PyErr_NewException("engine.ShaderError", NULL, NULL);
    Py_INCREF(ShaderError);
    PyModule_AddObject(m, "ShaderError", ShaderError);
}

