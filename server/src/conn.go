package conn

import (
	"os"
	"fmt"
	"net"
	"time"
	"bytes"
//	"strconv"
	"hash/fnv"
	"encoding/binary"
)

type Session struct {
	Addr *net.UDPAddr
	SessionID uint16
	Sequencer uint16
	LastKeepAlive uint64
	RTTWindow [256]int64
}

type Packet struct {
	ProtoID uint32
	Sequence uint16
	Ack uint16
	AckBitfield uint32
	MsgType	uint32
	Data []byte
}

func NewPacket(ba []byte) (p Packet) {
	binary.Read(bytes.NewBuffer(ba), binary.LittleEndian, &p);
	return p;
}

func (p *Packet) MatchesProtocol(game_name, protocol_version string) bool {
	hasher := fnv.New32a();
	hasher.Write([]byte(game_name));
	hasher.Write([]byte(protocol_version));
	return (p.ProtoID == hasher.Sum32());
}

func sequenceMoreRecent(s1, s2 uint16) bool {
	return (s1 > s2) && (s1 - s2 <= 0x7FFF) ||
		   (s2 > s1) && (s2 - s1 >  0x7FFF);
}

var Connections map[*net.UDPAddr]Session;

func NetLoop(ip, port string) {
	// Set up the bytes udp packets must have prefixed
    game_name := "Dwarvenly";
    proto_v := "0.0";
    proto_hasher := fnv.New32a();
    proto_hasher.Write([]byte(game_name));
    proto_hasher.Write([]byte(proto_v));
    proto_hash := proto_hasher.Sum32();
	proto_hashb := bytes.NewBuffer([]byte{});
	binary.Write(proto_hashb, binary.LittleEndian, proto_hash);

    // Set up udp listening
    laddr, err := net.ResolveUDPAddr("udp", ip+":"+port)
    if err != nil {
		fmt.Printf("Resolving failed: %s\n", err); os.Exit(1);
	}

    c, err := net.ListenUDP("udp", laddr);
    if err != nil {
		fmt.Printf("Listening failed: %s\n", err); os.Exit(1);
	}

    err = c.SetTimeout(1000000) // 1ms timeout
    if err != nil {
		fmt.Printf("Setting timeout failed: %s\n", err); os.Exit(1);
	}

    in := make([]byte, 4096)
    //now := time.Nanoseconds()

	var curr int64 = 0;
	var prev int64 = 0;
	var dt int64 = 0;

	for {
		// Time
		prev = curr;
		curr = time.Nanoseconds()
		dt = curr - prev

		// Get from clients
        in_count, addr, err := c.ReadFromUDP(in[:])
        if err != nil {
            if e, ok := err.(*net.OpError); ok {
                if e.Timeout() {
                    continue
                }
                fmt.Printf("Read error: %v\n", e)
            }
        } else {
/*
			session, ok := Connections[addr]
            if !ok {
                Connections[addr] = Session{}
				session := Connections[addr]
            }
*/
            fmt.Printf("at %s %#+v\n", dt, in[:in_count])
            // JSON interpret the data
/*
			err := json.Unmarshal(in[:in_count], &inp)
            if err != nil {
                fmt.Printf("JSON decode error: %s\n", err)
            }
            fmt.Printf("!!! %#+v\n", inp)
            session.handle(inp)
*/
            fmt.Printf("%s\n", in[:in_count])

            // Echo
			_, erk := c.WriteToUDP(in[:], addr)
            if erk != nil {
                if e, ok := err.(*net.OpError); ok {
                    if !e.Timeout() {
                        fmt.Printf("Read error: %v\n", e)
                    }
                }
            }
        }


	}
}
