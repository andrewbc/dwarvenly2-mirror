package maps

type Map struct {
    Seed        int64
    Name        string
    Description string
    Version     string
    Format      string
}

type MapsInfo struct {
    MapCount    uint64
    Current     string
}

const (
    Air = iota
    Dirt
    Stone
    reserved
    
    Sand = 128
    Tundra
    Snow
    Bog
)

func getSeedFromCoord(map_seed, x, y, z int64) int64 {
    //new_x := x & 0x8aaaaaa8aaaa8aaa // select wanted bits from x, 0's to prepare for z merge
    //tmp_xz := new_x ^ (new_x ^ z) & 0x2000400200402001) // merge x and z together
    //return map_seed ^ (tmp_xz ^ (tmp_xz ^ y) & 0x5555155555155514))  // merge y with xz, integrate map seed
    // XXXX XXXX  XXXX XXXX  XXXX XXXX  XXXX XXXX    XXXX XXXX  XXXX XXXX  XXXX XXXX  XXXX XXXX =Z
    //   ^                    ^                ^                 ^           ^                ^
    // 0x2000400200402001
    // 
    // XXXX XXXX  XXXX XXXX  XXXX XXXX  XXXX XXXX    XXXX XXXX  XXXX XXXX  XXXX XXXX  XXXX XXXX =Y
    //  ^ ^  ^ ^   ^ ^  ^ ^     ^  ^ ^   ^ ^  ^ ^     ^ ^  ^ ^     ^  ^ ^   ^ ^  ^ ^   ^ ^  ^                       
    // 0x5555155555155514                                                                                        
    // 
    // XXXX XXXX  XXXX XXXX  XXXX XXXX  XXXX XXXX    XXXX XXXX  XXXX XXXX  XXXX XXXX  XXXX XXXX =X
    // ^    ^ ^   ^ ^  ^ ^   ^ ^  ^ ^   ^ ^  ^       ^ ^  ^ ^   ^ ^  ^ ^   ^    ^ ^   ^ ^  ^ ^                                                                                         
    // 0x8aaaaaa8aaaa8aaa
    
    // Let's try something more simple first.
    return map_seed ^ x ^ y ^ z
}