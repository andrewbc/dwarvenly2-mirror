package maps

import (
    "io/ioutil"
    "os"
    "fmt"
    "json"
    "strconv"
    "rand"
	"launchpad.net/gommap"
//    "bytes"

    "physics"
)

func Setup(name, description, version string, seed int64) {
    // Check for maps dir
    fmt.Printf("Testing maps dir... ")
    maps_dir, err := os.Lstat("maps")
    if err != nil {
        fmt.Fprintf(os.Stderr, "\nWhere is the fscking maps dir? %s\nAttempting to create one...\n", err.String())
        err = os.Mkdir("maps", 0744)
        if err != nil {
            fmt.Fprintf(os.Stderr, "That didn't fscking work! %s\n", err.String())
            os.Exit(1)
        }
    } else if !maps_dir.IsDirectory() {
        fmt.Fprintf(os.Stderr, "\nThere's a maps something, but it's not a fscking directory.\n")
        os.Exit(1)
    }
    fmt.Printf("ok.\n")

    // Check for general json info file
    fmt.Printf("Testing maps json... ")
    info_json, err := os.Lstat("maps/info.json")
    if err != nil {
        fmt.Fprintf(os.Stderr, "\nProblem checking for maps/info.json: %s\n", err.String())
        os.Exit(1)
    } else if !info_json.IsRegular() {
        fmt.Fprintf(os.Stderr, "\nThe maps/info.json file is not a regular file...\n")
        os.Exit(1)
    }
    fmt.Printf("ok.\n")

    // Open maps general json info file
    fmt.Printf("Testing general json... ")
    f, err := os.Open("maps/info.json")
    buf, err := ioutil.ReadAll(f)
    if err != nil {
        fmt.Fprintf(os.Stderr, "\nError reading from %s: %s\n", f.Name(), err.String())
        os.Exit(1)
    }
    fmt.Printf("ok.\n")

    // Parse JSON info
    fmt.Printf("Using general json... ")
    var s MapsInfo
    erj := json.Unmarshal(buf, &s)
    if erj != nil {
        fmt.Fprintf(os.Stderr, "\nError unJSONing from %s: %s\n", f.Name(), erj.String())
        os.Exit(1)
    }

    // Fetch and increase map count
    s.MapCount += 1

    // Set name to N+1 if there's no explicit name
    if len(name) < 1 {
        name = strconv.Uitoa64(s.MapCount)
    }
    fmt.Printf("ok.\n")

    // Check whether the map name exists
    fmt.Printf("Testing particular map dir... ")
    map_dir, err := os.Lstat("maps/"+name)
    if err != nil {
        fmt.Fprintf(os.Stderr, "\nProblem reading the dir for map \"%s\"? %s\n", name, err.String())
        os.Exit(1)
    } else if !map_dir.IsDirectory() {
        fmt.Fprintf(os.Stderr, "\nThere's a maps something, but it's not a fscking directory.\n")
        os.Exit(1)
    }
    fmt.Printf("ok.\nGenerating first zone...")

    Generate("maps/"+name, 0, 0, 0)
    fmt.Printf("ok.\n")
}

func FileExists(name string) (bool, os.Error) {
        _, err := os.Stat(name)
        if err == nil {
            return true, nil
        }
        // Check if error is "no such file or directory"
        if e, ok := err.(*os.PathError); ok && e.Error == os.ENOENT {
            return false, nil
        }
        return false, err
}

func Generate(dir string, x, y, z int64) {
    fname := dir+"/"+strconv.Itoa64(x)+":"+strconv.Itoa64(y)+":"+strconv.Itoa64(z)+".tiles"

    var f *os.File
    // Check for existence
    fstat, err := os.Stat(fname)

    if err != nil {
        // Check if error is "no such file or directory"
        if e, ok := err.(*os.PathError); ok && e.Error == os.ENOENT {
            f, err = os.Create(fname)
            fstat, err = os.Stat(fname)
            if err != nil {
                fmt.Fprintf(os.Stderr, "\nError generating map file for (x=%s, y=%s, z=%s): %s\n", x, y, z, err)
                os.Exit(-1)
            }
        } else {
            fmt.Fprintf(os.Stderr, "\nError generating map file for (x=%s, y=%s, z=%s): %s\n", x, y, z, err)
            os.Exit(-1)
        }
    }

    if !fstat.IsRegular() {
        f, err = os.Create(fname)
    } else {
        f, err = os.OpenFile(fname, os.O_RDWR, 0666)
    }

    if err != nil {
        fmt.Fprintf(os.Stderr, "\nError generating map file for (x=%s, y=%s, z=%s): %s\n", x, y, z, err)
        os.Exit(-1)
    }

    err = f.Truncate(512*128*128)
    if err != nil {
        fmt.Fprintf(os.Stderr, "\nError generating map file for (x=%s, y=%s, z=%s): %s\n", x, y, z, err)
        os.Exit(-1)
    }


    tiles, err := gommap.Map(f.Fd(), gommap.PROT_READ | gommap.PROT_WRITE, gommap.MAP_SHARED)
    if err != nil {
        fmt.Fprintf(os.Stderr, "\nError generating map file for (x=%s, y=%s, z=%s): %s\n", x, y, z, err)
        os.Exit(-1)
    }

    rand.Seed(getSeedFromCoord(0, x, y, z))

    // Terrain
    var rands = [4096]float64{}
    for i := 0; i < 4096; i++ {
        rands[i] = rand.Float64()
    }

    tmp := 0.0
    index := 0
    xx := 0
    yy := 0
    zz := 0

    // Top air shit
    for ; yy < 128; yy++ {
        for ; xx < 128; xx++ {
            for ; zz < 52; zz++ {
                index = yy*(512*128)+xx*512+zz
                tiles[index] = 0x00 // Air
            }
        }
    }

    for yy = 0; yy < 128; yy++ {
        for xx = 0; xx < 128; xx++ {
            for zz = 52; zz < 70; zz++ {
                tmp = physics.Simplex3(physics.Vector3{float64(xx), float64(yy), float64(zz)})
                index = yy*(512*128)+xx*512+zz
                if tmp <= 0.0 {
                    tiles[index] = 0x00 // Air
                } else {
                    tiles[index] = 0x01 // Dirt
                }
            }
        }
    }

    // bottom dirt shit
    for yy = 0; yy < 128; yy++ {
        for xx = 0; xx < 128; xx++ {
            for zz = 129; zz < 512; zz++ {
                index = yy*(512*128)+xx*512+zz
                tiles[index] = 0x01 // Dirt
            }
        }
    }
}
