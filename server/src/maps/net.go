package maps

import (
    vec "container/vector"
)

type TileSequence struct {
    Tile, Count uint8
}

type MapTiles struct {
    T           vec.Vector
    more        byte
    more_len    uint8
}

func (m MapTiles) CompressAdd(t_ byte) {
    if t_ >= 0x8 {
        m.T.Push(t_)
    } else {
        t := t_ & 0x07  // Ensure that only the bottom three bits are used for small tiles.
        switch (m.more_len) {
            case 0:
                m.more = t << 5
                m.more_len = 3
            case 3:
                m.more = m.more | t << 2
                m.more_len = 6;
            case 6:
                m.more = m.more | t >> 1
                m.T.Push(m.more)
                m.more = t << 7
                m.more_len = 1
            case 1:
                m.more = m.more | t << 4
                m.more_len = 4
            case 4:
                m.more = m.more | t << 1
                m.more_len = 7
            case 7:
                m.more = m.more | t >> 2
                m.T.Push(m.more)
                m.more = t << 6
                m.more_len = 2
            case 2:
                m.more = m.more | t << 3
                m.more_len = 5
            case 5:
                m.more = m.more | t
                m.T.Push(m.more)
                m.more = 0
                m.more_len = 0
        }
    }
}