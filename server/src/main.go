package main

import (
    "time"

    "physics"
//    "maps"
	"conn"
)

type Input struct {
    left    bool
    right   bool
    forward bool
    back    bool
    jump    bool
}


type Character struct {
    buffer []byte
    State physics.State
    Input Input
}

func (c Character) update(dt int64, input Input, state physics.State) {
    /*pos_diff := state.Position.Sub(c.State.Position)
    pos_diff_len := pos_diff.Len()
    if pos_diff_len > 2.0 {
        c.State.Position = state.Position
    } else if pos_diff_len > 0.1 {
        c.State.Position.IAdd(c.State.Position.MulF(0.1))
    }
    c.State.Velocity = state.Velocity
    c.Input = input*/
}

func (c Character) rpc(curr_t, t int64, input Input) {
    if (t-1000) < curr_t  { return }
    dt := curr_t - t
    c.update(dt, input, c.State)
}

func (c Character) handle(event map[string]interface{} ) {
    
}

func send_update(time int64, input Input, state physics.State) {
    
}

func SimLoop() {
	var prev, curr, dt int64 = 0, 0, 0;
	for {
		prev = curr
		curr = time.Nanoseconds();
		dt = curr - prev
		_ = dt
		// Simulate
	}
}

func main() {
	go conn.NetLoop("127.0.0.1", "44044");
    SimLoop();
}
