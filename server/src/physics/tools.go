package physics

var epsilon float64 = 0.00001                       ///< floating point epsilon for single precision. todo: verify epsilon value and usage
var epsilonSquared float64 = epsilon * epsilon      ///< epsilon value squared
var pi float64 = 3.1415926                          ///< pi stored at a reasonable precision for single precision floating point.

func FLerp(p1, p2, t float64) float64 {
    return p1 + t * (p2 - p1)
}

func FSlerp(p1, p2, t float64) float64 {
    return ((1 - t) * p1) + (t * p2)
}

func FScerp(p1, s1, s2, p2, t float64) float64 {
    return FSlerp(FSlerp(p1, p2, t), FSlerp(s1, s2, t), 2*t*(1-t))
}

func FastFloor(x float64) int64 {
    if x > 0 { return int64(x) }
    return int64(x-1)
}

