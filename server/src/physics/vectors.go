package physics

import ("math")

////////////////////////////////////////////////////////////////////////////////////////////////
// Vector2
////////////////////////////////////////////////////////////////////////////////////////////////
type Vector2 struct {
    X, Y    float64
}

func Vector2FromF(f float64) Vector2 {
    return Vector2{f, f}
}

func Vector2FromV3(v Vector3) Vector2 {
    return Vector2{v.X, v.Y}
}

func (v Vector2) IAddF(f float64) {
    v.X += f
    v.Y += f
}

func (v Vector2) IAdd(v2 Vector2) {
    v.X += v2.X
    v.Y += v2.Y
}

func (v Vector2) ISubF(f float64) {
    v.X -= f
    v.Y -= f
}

func (v Vector2) ISub(v2 Vector2) {
    v.X -= v2.X
    v.Y -= v2.Y
}

func (v Vector2) IMulF(f float64) {
    v.X *= f
    v.Y *= f
}

func (v Vector2) IMul(v2 Vector2) {
    v.X *= v2.X
    v.Y *= v2.Y
}

func (v Vector2) IDivF(f float64) {
    v.X /= f
    v.Y /= f
}

func (v Vector2) IDiv(v2 Vector2) {
    v.X /= v2.X
    v.Y /= v2.Y
}

func (v Vector2) Len() float64 {
    return math.Sqrt(v.Dot(v))
}

func (v1 Vector2) AddF(f float64) (v Vector2) {
    v.X = v1.X + f
    v.Y = v1.Y + f
    return v
}

func (v1 Vector2) Add(v2 Vector3) (v Vector2) {
    v.X = v1.X + v2.X
    v.Y = v1.Y + v2.Y
    return v
}

func (v1 Vector2) SubF(f float64) (v Vector2) {
    v.X = v1.X - f
    v.Y = v1.Y - f
    return v
}


func (v1 Vector2) Sub(v2 Vector2) (v Vector2) {
    v.X = v1.X - v2.X
    v.Y = v1.Y - v2.Y
    return v
}

func (v1 Vector2) MulF(f float64) (v Vector2) {
    v.X = v1.X * f
    v.Y = v1.Y * f
    return v
}

func (v1 Vector2) Mul(v2 Vector2) (v Vector2) {
    v.X = v1.X * v2.X
    v.Y = v1.Y * v2.Y
    return v
}

func (v1 Vector2) DivF(f float64) (v Vector2) {
    v.X = v1.X / f
    v.Y = v1.Y / f
    return v
}

func (v1 Vector2) Div(v2 Vector2) (v Vector2) {
    v.X = v1.X / v2.X
    v.Y = v1.Y / v2.Y
    return v
}

func (v1 Vector2) PowF(f float64) (v Vector2) {
    v.X = math.Pow(v1.X, f)
    v.Y = math.Pow(v1.Y, f)
    return v
}

func (v1 Vector2) Pow(v2 Vector2) (v Vector2) {
    v.X = math.Pow(v1.X, v2.X)
    v.Y = math.Pow(v1.Y, v2.Y)
    return v
}

func (v1 Vector2) INeg() {
    v1.X = -v1.X
    v1.Y = -v1.Y
}

func (v1 Vector2) Neg() (v Vector2) {
    v.X = -v1.X
    v.Y = -v1.Y
    return v   
}

func (v1 Vector2) Sum() float64 {
    return v1.X+v1.Y
}

func (v1 Vector2) Dot(v2 Vector2) float64 {
   return v1.Mul(v2).Sum()
}

func (v1 Vector2) INorm() {
    v1.IDivF(v1.Len())
}
func (v1 Vector2) Norm() (v Vector2) {
    return v1.DivF(v1.Len())
}

func (v1 Vector2) Dist(v2 Vector2) float64 {
    return math.Sqrt(v2.Sub(v1).PowF(2.0).Sum())
}

func (v1 Vector2) ISqrt() {
    v1.X = math.Sqrt(v1.X)
    v1.Y = math.Sqrt(v1.Y)
}

func (v1 Vector2) Sqrt() (v Vector2) {
    v.X = math.Sqrt(v1.X)
    v.Y = math.Sqrt(v1.Y)
    return v
}

////////////////////////////////////////////////////////////////////////////////////////////////
// Vector3
////////////////////////////////////////////////////////////////////////////////////////////////
type Vector3 struct {
    X, Y, Z float64
}

func Vector3FromV2 (v Vector2) Vector3 {
    return Vector3{v.X, v.Y, 0.0}
}

func Vector3FromQ (q Quaternion) Vector3 {
    return Vector3{q.X, q.Y, q.Z}
}

func (v Vector3) IAddF(f float64) {
    v.X += f
    v.Y += f
    v.Z += f
}

func (v Vector3) IAdd(v2 Vector3) {
    v.X += v2.X
    v.Y += v2.Y
    v.Z += v2.Z
}

func (v Vector3) ISubF(f float64) {
    v.X -= f
    v.Y -= f
    v.Z -= f
}

func (v Vector3) ISub(v2 Vector3) {
    v.X -= v2.X
    v.Y -= v2.Y
    v.Z -= v2.Z
}

func (v Vector3) IMulF(f float64) {
    v.X *= f
    v.Y *= f
    v.Z *= f
}

func (v Vector3) IMul(v2 Vector3) {
    v.X *= v2.X
    v.Y *= v2.Y
    v.Z *= v2.Z
}

func (v Vector3) IDivF(f float64) {
    v.X /= f
    v.Y /= f
    v.Z /= f
}

func (v Vector3) IDiv(v2 Vector3) {
    v.X /= v2.X
    v.Y /= v2.Y
    v.Z /= v2.Z
}

func (v Vector3) LenSquared() float64 {
    return v.Dot(v)
}

func (v Vector3) Len() float64 {
    return math.Sqrt(v.Dot(v))
}

func (v1 Vector3) AddF(f float64) (v Vector3) {
    v.X = v1.X + f
    v.Y = v1.Y + f
    v.Z = v1.Z + f
    return v
}

func (v1 Vector3) Add(v2 Vector3) (v Vector3) {
    v.X = v1.X + v2.X
    v.Y = v1.Y + v2.Y
    v.Z = v1.Z + v2.Z
    return v
}

func (v1 Vector3) SubF(f float64) (v Vector3) {
    v.X = v1.X - f
    v.Y = v1.Y - f
    v.Z = v1.Z - f
    return v
}


func (v1 Vector3) Sub(v2 Vector3) (v Vector3) {
    v.X = v1.X - v2.X
    v.Y = v1.Y - v2.Y
    v.Z = v1.Z - v2.Z
    return v
}

func (v1 Vector3) MulF(f float64) (v Vector3) {
    v.X = v1.X * f
    v.Y = v1.Y * f
    v.Z = v1.Z * f
    return v
}

func (v1 Vector3) Mul(v2 Vector3) (v Vector3) {
    v.X = v1.X * v2.X
    v.Y = v1.Y * v2.Y
    v.Z = v1.Z * v2.Z
    return v
}

func (v1 Vector3) DivF(f float64) (v Vector3) {
    v.X = v1.X / f
    v.Y = v1.Y / f
    v.Z = v1.Z / f
    return v
}

func (v1 Vector3) Div(v2 Vector3) (v Vector3) {
    v.X = v1.X / v2.X
    v.Y = v1.Y / v2.Y
    v.Z = v1.Z / v2.Z
    return v
}

func (v1 Vector3) PowF(f float64) (v Vector3) {
    v.X = math.Pow(v1.X, f)
    v.Y = math.Pow(v1.Y, f)
    v.Z = math.Pow(v1.Z, f)
    return v
}

func (v1 Vector3) Pow(v2 Vector3) (v Vector3) {
    v.X = math.Pow(v1.X, v2.X)
    v.Y = math.Pow(v1.Y, v2.Y)
    v.Z = math.Pow(v1.Z, v2.Z)
    return v
}

func (v1 Vector3) INeg() {
    v1.X = -v1.X
    v1.Y = -v1.Y
    v1.Z = -v1.Z
}

func (v1 Vector3) Neg() (v Vector3) {
    v.X = -v1.X
    v.Y = -v1.Y
    v.Z = -v1.Z
    return v   
}

func (v1 Vector3) Sum() float64 {
    return v1.X+v1.Y+v1.Z
}

func (v1 Vector3) Dot(v2 Vector3) float64 {
   return v1.Mul(v2).Sum()
}

func (v1 Vector3) INorm() {
    v1.IDivF(v1.Len())
}
func (v1 Vector3) Norm() (v Vector3) {
    return v1.DivF(v1.Len())
}

func (v1 Vector3) Dist(v2 Vector3) float64 {
    return math.Sqrt(v2.Sub(v1).PowF(2.0).Sum())
}

func (v1 Vector3) ISqrt() {
    v1.X = math.Sqrt(v1.X)
    v1.Y = math.Sqrt(v1.Y)
    v1.Z = math.Sqrt(v1.Z)
}

func (v1 Vector3) Sqrt() (v Vector3) {
    v.X = math.Sqrt(v1.X)
    v.Y = math.Sqrt(v1.Y)
    v.Z = math.Sqrt(v1.Z)
    return v
}

func (v1 Vector3) Cross(v2 Vector3) (v Vector3) {
    v.X = v1.Y*v2.Z - v1.Z*v2.Y 
    v.Y = v1.Z*v2.X - v1.X*v2.Z
    v.Z = v1.X*v2.Y - v1.Y*v2.X
    return v
}

func (v1 Vector3) ITorque(state *State, t float64) {
    v1.X = 1
    v1.Y = 0
    v1.Z = 0
    v1.ISub(state.AngularVelocity)
    v1.IMulF(0.1)
}
