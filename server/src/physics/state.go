package physics

import (
//  "math"
)


////////////////////////////////////////////////////////////////////////////////////////////////
// Game state
////////////////////////////////////////////////////////////////////////////////////////////////
type State struct {
    // primary
    Position        Vector3
    Momentum        Vector3
    Orientation     Quaternion
    AngularMomentum Vector3
    
    // secondary
    Velocity        Vector3
    Spin            Quaternion
    AngularVelocity Vector3
    
    
    // constant
    Size        float64
    Mass        float64
    MassInv     float64       
    Inertia     float64
    InertiaInv  float64
}

func (s *State) Recalculate() {
    s.Velocity = s.Momentum.MulF(s.MassInv)
    s.AngularVelocity = s.AngularMomentum.MulF(s.InertiaInv)
    s.Orientation.INorm()
    s.Spin.W = 0
    s.Spin.X = s.AngularVelocity.X * 0.5
    s.Spin.Y = s.AngularVelocity.Y * 0.5
    s.Spin.Z = s.AngularVelocity.Z * 0.5
    s.Spin.IMul(s.Orientation)
}

func (s *State) Torque(t float64) Vector3 {
    v := Vector3{1.0, 0.0, 0.0}
    v.ISub(s.AngularVelocity.MulF(0.1))
    return v
}

type Derivative struct {
    Spin    Quaternion
    Torque  Vector3
}

func Evaluate(state, deriv *State, t, dt float64) *State {
    interim := new(State)
    interim.Orientation = state.Orientation.Add(deriv.Orientation.MulF(dt))
    interim.AngularVelocity = state.AngularVelocity.Add(deriv.AngularVelocity.MulF(dt))
    
    result := new(State)
    result.Orientation = Quaternion{0,
                                    interim.AngularVelocity.X,
                                    interim.AngularVelocity.Y,
                                    interim.AngularVelocity.Z}
    result.AngularVelocity = Vector3FromQ(Acceleration(interim, t+dt))
    return result
}

func Acceleration(s *State, t float64) Quaternion {
    return s.Orientation.MulF(-10).Sub(QuaternionFromV3(s.AngularVelocity))
}

func Integrate(s *State, t, dt float64) Quaternion {
    a := Evaluate(s, new(State),        t,    0.0)
    b := Evaluate(s,          a, t+dt*0.5, dt*0.5)
    c := Evaluate(s,          b, t+dt*0.5, dt*0.5)
    d := Evaluate(s,          c, t+dt    , dt    )
    
    return a.Orientation.Add(d.Orientation).Add(b.Orientation.Add(c.Orientation).MulF(2.0)).MulF(1.0/6.0)
    
}