package physics

import ("math")

////////////////////////////////////////////////////////////////////////////////////////////////
// Quaternions
////////////////////////////////////////////////////////////////////////////////////////////////
type Quaternion struct {
    W, X, Y, Z float64
}

func QuaternionFromV3 (v Vector3) Quaternion {
    return Quaternion{0.0, v.X, v.Y, v.Z}
}

func QuaternionFromV2 (v Vector2) Quaternion {
    return Quaternion{0.0, v.X, v.Y, 0.0}
}

func (q Quaternion) IAddF(f float64) {
    q.W += f
    q.X += f
    q.Y += f
    q.Z += f
}

func (q Quaternion) ISubF(f float64) {
    q.W -= f
    q.X -= f
    q.Y -= f
    q.Z -= f
}

func (q Quaternion) Len() float64 {
    return math.Sqrt(q.W*q.W + q.X*q.X + q.Y*q.Y + q.Z*q.Z)
}

func (q Quaternion) IMul(q1 Quaternion) {
    q.W = q.W*q1.W - q.X*q1.X - q.Y*q1.Y - q.Z*q1.Z
    q.X = q.W*q1.X + q.X*q1.W - q.Y*q1.Z + q.Z*q1.Y
    q.Y = q.W*q1.Y + q.X*q1.Z + q.Y*q1.W - q.Z*q1.X
    q.Z = q.W*q1.Z - q.X*q1.Y + q.Y*q1.X + q.Z*q1.W
}

func (q Quaternion) IMulF(f float64) {
    q.W *= f
    q.X *= f
    q.Y *= f
    q.Z *= f
}

func (q Quaternion) IDivF(f float64) {
    q.W /= f
    q.X /= f
    q.Y /= f
    q.Z /= f
}

func (q Quaternion) Sum() float64 {
    return q.W + q.X + q.Y + q.Z
}

func (q Quaternion) IPow(f float64) {
    q.W = math.Pow(q.W, f)
    q.X = math.Pow(q.X, f)
    q.Y = math.Pow(q.Y, f)
    q.Z = math.Pow(q.Z, f)
}

func (q Quaternion) INorm() {
    q.IDivF(q.Len())
}

func (q1 Quaternion) AddF(f float64) (q Quaternion) {
    q.W = q1.W + f
    q.X = q1.X + f
    q.Y = q1.Y + f
    q.Z = q1.Z + f
    return q
}

func (q1 Quaternion) Add(q2 Quaternion) (q Quaternion) {
    q.W = q1.W + q2.W
    q.X = q1.X + q2.X
    q.Y = q1.Y + q2.Y
    q.Z = q1.Z + q2.Z
    return q
}

func (q1 Quaternion) SubF(f float64) (q Quaternion) {
    q.W = q1.W - f
    q.X = q1.X - f
    q.Y = q1.Y - f
    q.Z = q1.Z - f
    return q
}

func (q1 Quaternion) Sub(q2 Quaternion) (q Quaternion) {
    q.W = q1.W - q2.W
    q.X = q1.X - q2.X
    q.Y = q1.Y - q2.Y
    q.Z = q1.Z - q2.Z
    return q
}

func (q1 Quaternion) MulF(f float64) (q Quaternion) {
    q.W = q1.W * f
    q.X = q1.X * f
    q.Y = q1.Y * f
    q.Z = q1.Z * f
    return q
}

func (q1 Quaternion) Mul(q2 Quaternion) (q Quaternion) {
    q.W = q1.W*q2.W - q1.X*q2.X - q1.Y*q2.Y - q1.Z*q2.Z
    q.X = q1.W*q2.X + q1.X*q2.W - q1.Y*q2.Z + q1.Z*q2.Y
    q.Y = q1.W*q2.Y + q1.X*q2.Z + q1.Y*q2.W - q1.Z*q2.X
    q.Z = q1.W*q2.Z - q1.X*q2.Y + q1.Y*q2.X + q1.Z*q2.W
    return q
}

func (q1 Quaternion) DivF(f float64) (q Quaternion) {
    q.W = q1.W / f
    q.X = q1.X / f
    q.Y = q1.Y / f
    q.Z = q1.Z / f
    return q
}

func (q1 Quaternion) Div(q2 Quaternion) (q Quaternion) {
    quotient := q2.W*q2.W + q2.X*q2.X + q2.Y*q2.Y + q2.Z*q2.Z
    w_ := q1.W*q2.W
    x_ := q1.X*q2.X
    y_ := q1.Y*q2.Y
    z_ := q1.Z*q2.Z
    q.W = (w_ + x_ + y_ + z_)  /  quotient
    q.X = (w_ - x_ - y_ + z_)  /  quotient
    q.Y = (w_ + x_ - y_ - z_)  /  quotient
    q.Z = (w_ - x_ + y_ - z_)  /  quotient
    return q
}

func (q1 Quaternion) Conj() (q Quaternion) {
    q.W = q1.W
    q.X = -q1.X
    q.Y = -q1.Y
    q.Z = -q1.Z
    return q
}

func (q1 Quaternion) Inv() (q Quaternion) {
    conj := q1.Conj()
    return conj.Div(conj.Mul(q1))
}

func (q1 Quaternion) Norm() Quaternion {
    return q1.DivF(q1.Len())
}

func (q1 Quaternion) Dot(q2 Quaternion) float64 {
    return q1.Mul(q2).Sum()
}

func (q1 Quaternion) RotateOn(v1 Vector3) (v Vector3) {
    q2 := Quaternion{0, v1.X, v1.Y, v1.Z}.Mul(q1.Conj())
    v.X = q2.X
    v.Y = q2.Y
    v.Z = q2.Z
    return v
}


////////////////////////////////////////////////////////////////////////////////////////////////
// Interpolations
////////////////////////////////////////////////////////////////////////////////////////////////
func (q1 Quaternion) Lerp(q2 Quaternion, t float64) (q Quaternion) {
    return q1.MulF(1-t).Add(q2.MulF(t))
}

func (q1 Quaternion) Slerp(q2 Quaternion, t float64) (q Quaternion) {
    // Quaternion Spherical Linear Interpolation
    cos_half_theta := q1.Dot(q2) 
    if cos_half_theta < 0 {
        q2.W = -q2.W
        q2.X = -q2.X
        q2.Y = -q2.Y
        q2.Z = q2.Z
    }
    
    if math.Fabs(cos_half_theta) >= 1.0 {
        q.W = q1.W
        q.X = q1.X
        q.Y = q1.Y
        q.Z = q1.Z
        return q
    }
    
    half_theta := math.Acos(cos_half_theta)
    sin_half_theta := math.Sqrt(1.0 - cos_half_theta*cos_half_theta)
    
    if math.Fabs(sin_half_theta) < 0.001 {
        q.W = q1.W*0.5 + q2.W*0.5
        q.X = q1.X*0.5 + q2.X*0.5
        q.Y = q1.Y*0.5 + q2.Y*0.5
        q.Z = q1.Z*0.5 + q2.Z*0.5
        return q
    }
    
    ratio1 := math.Sin((1-t) * half_theta) / sin_half_theta
    ratio2 := math.Sin(t * half_theta) / sin_half_theta
    
    q.W = q1.W*ratio1 + q2.W*ratio2
    q.X = q1.X*ratio1 + q2.X*ratio2
    q.Y = q1.Y*ratio1 + q2.Y*ratio2
    q.Z = q1.Z*ratio1 + q2.Z*ratio2
    return q
}

func (q1 Quaternion) SlerpNoInvert(q2 Quaternion, t float64) (q Quaternion) {
    cos_half_theta := q1.Dot(q2)
    half_theta := math.Acos(cos_half_theta)
    sin_half_theta := math.Sqrt(1.0 - cos_half_theta*cos_half_theta)
    ratio1 := math.Sin((1-t) * half_theta) / sin_half_theta
    ratio2 := math.Sin(t * half_theta) / sin_half_theta

    if math.Fabs(sin_half_theta) < 0.001 {
        q.W = q1.W*0.5 + q2.W*0.5
        q.X = q1.X*0.5 + q2.X*0.5
        q.Y = q1.Y*0.5 + q2.Y*0.5
        q.Z = q1.Z*0.5 + q2.Z*0.5
        return q
    }
    
    q.W = q1.W*ratio1 + q2.W*ratio2
    q.X = q1.X*ratio1 + q2.X*ratio2
    q.Y = q1.Y*ratio1 + q2.Y*ratio2
    q.Z = q1.Z*ratio1 + q2.Z*ratio2
    return q
}

func (q1 Quaternion) Scerp(a, b, q2 Quaternion, t float64) Quaternion {
    // Quaternion Spherical Cubic Interpolation
    return q1.SlerpNoInvert(q2, t).SlerpNoInvert(a.SlerpNoInvert(b, t), 2*t*(1-t))
}

func (q1 Quaternion) Bezier(q2, a, b Quaternion, t float64) Quaternion {
    r1 := q1.SlerpNoInvert( a, t)
    r2 :=  a.SlerpNoInvert( b, t)
    r3 :=  b.SlerpNoInvert(q2, t)
    return r1.SlerpNoInvert(r2, t).SlerpNoInvert(r2.SlerpNoInvert(r3, t), t)
}
