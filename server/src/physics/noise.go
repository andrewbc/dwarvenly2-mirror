package physics

import (
    "math"
)

var grad3f = [12] Vector3{ Vector3{1.0, 1.0, 0.0}, Vector3{-1.0, 1.0, 0.0}, Vector3{1.0,-1.0, 0.0}, Vector3{-1.0,-1.0, 0.0},
                           Vector3{1.0, 0.0, 1.0}, Vector3{-1.0, 0.0, 1.0}, Vector3{1.0, 0.0,-1.0}, Vector3{-1.0, 0.0,-1.0},
                           Vector3{0.0, 1.0, 1.0}, Vector3{ 0.0,-1.0, 1.0}, Vector3{0.0, 1.0,-1.0}, Vector3{ 0.0,-1.0,-1.0}}

var grad4f = [32] Quaternion{ Quaternion{ 0.0, 1.0, 1.0, 1.0}, Quaternion{ 0.0, 1.0, 1.0,-1.0}, Quaternion{ 0.0, 1.0,-1.0, 1.0}, Quaternion{ 0.0, 1.0,-1.0,-1.0},
                              Quaternion{ 0.0,-1.0, 1.0, 1.0}, Quaternion{ 0.0,-1.0, 1.0,-1.0}, Quaternion{ 0.0,-1.0,-1.0, 1.0}, Quaternion{ 0.0,-1.0,-1.0,-1.0},
                              Quaternion{ 1.0, 0.0, 1.0, 1.0}, Quaternion{ 1.0, 0.0, 1.0,-1.0}, Quaternion{ 1.0, 0.0,-1.0, 1.0}, Quaternion{ 1.0, 0.0,-1.0,-1.0},
                              Quaternion{-1.0, 0.0, 1.0, 1.0}, Quaternion{-1.0, 0.0, 1.0,-1.0}, Quaternion{-1.0, 0.0,-1.0, 1.0}, Quaternion{-1.0, 0.0,-1.0,-1.0},
                              Quaternion{ 1.0, 1.0, 0.0, 1.0}, Quaternion{ 1.0, 1.0, 0.0,-1.0}, Quaternion{ 1.0,-1.0, 0.0, 1.0}, Quaternion{ 1.0,-1.0, 0.0,-1.0},
                              Quaternion{-1.0, 1.0, 0.0, 1.0}, Quaternion{-1.0, 1.0, 0.0,-1.0}, Quaternion{-1.0,-1.0, 0.0, 1.0}, Quaternion{-1.0,-1.0, 0.0,-1.0},
                              Quaternion{ 1.0, 1.0, 1.0, 0.0}, Quaternion{ 1.0, 1.0,-1.0, 0.0}, Quaternion{ 1.0,-1.0, 1.0, 0.0}, Quaternion{ 1.0,-1.0,-1.0, 0.0},
                              Quaternion{-1.0, 1.0, 1.0, 0.0}, Quaternion{-1.0, 1.0,-1.0, 0.0}, Quaternion{-1.0,-1.0, 1.0, 0.0}, Quaternion{-1.0,-1.0,-1.0, 0.0}}

var simplex = [64][4]int64{{0,1,2,3},{0,1,3,2},{0,0,0,0},{0,2,3,1},{0,0,0,0},{0,0,0,0},{0,0,0,0},{1,2,3,0},
                           {0,2,1,3},{0,0,0,0},{0,3,1,2},{0,3,2,1},{0,0,0,0},{0,0,0,0},{0,0,0,0},{1,3,2,0},
                           {0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},
                           {1,2,0,3},{0,0,0,0},{1,3,0,2},{0,0,0,0},{0,0,0,0},{0,0,0,0},{2,3,0,1},{2,3,1,0},
                           {1,0,2,3},{1,0,3,2},{0,0,0,0},{0,0,0,0},{0,0,0,0},{2,0,3,1},{0,0,0,0},{2,1,3,0},
                           {0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},
                           {2,0,1,3},{0,0,0,0},{0,0,0,0},{0,0,0,0},{3,0,1,2},{3,0,2,1},{0,0,0,0},{3,1,2,0},
                           {2,1,0,3},{0,0,0,0},{0,0,0,0},{0,0,0,0},{3,1,0,2},{0,0,0,0},{3,2,0,1},{3,2,1,0}}

var p = [512]int64{151,160,137,91,90,15,131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,
                   8,99,37,240,21, 10,23,190, 6,148,247,120,234,75, 0,26,197,62,94,252,219,203,
                   117,35, 11,32,57,177, 33, 88,237,149,56,87,174,20,125,136,171,168,68,175,74,
                   165,71,134,139,48,27, 166,77,146,158,231,83,111,229,122,60,211, 133,230,220,
                   105,92,41,55,46,245, 40,244,102,143,54,65,25,63,161,1,216, 80,73,209,76,132,
                   187,208,89,18,169,200,196,135,130,116,188,159,86,164, 100,109,198,173,186,3,
                   64,52,217,226,250,124,123,5,202,38,147,118,126,255,82,85,212,207,206,59,227,
                   47,16,58,17,182,189,28, 42,223,183,170,213,119,248,152,2,44,154,163, 70,221,
                   153,101,155,167,43,172,9,129,22,39,253,19,98,108,110,79,113,224,232,178,185,
                   112,104,218,246,97,228,251,34,242,193,238,210, 144,12,191,179,162,241,81,51,
                   145,235,249,14,239,107,49,192,214,31,181,199,106,157,184,84,204,176,115,121,
                   50,45,127, 4,150,254,138,236,205,93, 222,114,67,29,24,72,243,141,128,195,78,
                   66,215,61,156,180,
                   151,160,137,91,90,15,131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,
                   8,99,37,240,21, 10,23,190, 6,148,247,120,234,75, 0,26,197,62,94,252,219,203,
                   117,35, 11,32,57,177, 33, 88,237,149,56,87,174,20,125,136,171,168,68,175,74,
                   165,71,134,139,48,27, 166,77,146,158,231,83,111,229,122,60,211, 133,230,220,
                   105,92,41,55,46,245, 40,244,102,143,54,65,25,63,161,1,216, 80,73,209,76,132,
                   187,208,89,18,169,200,196,135,130,116,188,159,86,164, 100,109,198,173,186,3,
                   64,52,217,226,250,124,123,5,202,38,147,118,126,255,82,85,212,207,206,59,227,
                   47,16,58,17,182,189,28, 42,223,183,170,213,119,248,152,2,44,154,163, 70,221,
                   153,101,155,167,43,172,9,129,22,39,253,19,98,108,110,79,113,224,232,178,185,
                   112,104,218,246,97,228,251,34,242,193,238,210, 144,12,191,179,162,241,81,51,
                   145,235,249,14,239,107,49,192,214,31,181,199,106,157,184,84,204,176,115,121,
                   50,45,127, 4,150,254,138,236,205,93, 222,114,67,29,24,72,243,141,128,195,78,
                   66,215,61,156,180}
var (
    f2 = 0.5 * (math.Sqrt(3.0) - 1.0)
    g2 = (3.0 - math.Sqrt(3.0)) / 6.0
    f3 = 1.0 / 3.0
    g3 = 1.0 / 6.0
    f4 = (math.Sqrt(5.0) - 1.0) /  4.0
    g4 = (5.0 - math.Sqrt(5.0)) / 20.0
)

func fade(t float64) float64 {
   return t * t * t * (t * (t * 6 - 15) + 10);
}

func grad(hash int64, x, y, z float64) float64 {
    h := hash & 15
    u := y
    if h < 8 { u = x }
    
    v := y
    if h >= 4 {
        if h == 12 || h == 14 {
            v = y
        } else {
            v = z
        }
    }
    
    if h&1 != 0 { u = -u }
    if h&2 != 0 { v = -v }
    
    return u + v
}

func Perlin3(v3 Vector3) float64 {
    X  := int64(math.Floor(v3.X)) & 255
    Y  := int64(math.Floor(v3.Y)) & 255
    Z  := int64(math.Floor(v3.Z)) & 255
    x  := v3.X - math.Floor(v3.X)
    y  := v3.Y - math.Floor(v3.Y)
    z  := v3.Z - math.Floor(v3.Z)
    u  := fade(x)
    v  := fade(y)
    w  := fade(z)
    A  := p[X  ]+Y
    AA := p[A  ]+Z
    AB := p[A+1]+Z
    B  := p[X+1]+Y
    BA := p[B  ]+Z
    BB := p[B+1]+Z
    return FLerp(w, FLerp(v, FLerp(u,
                                                     grad(p[AA  ], x  , y  , z  ),
                                                     grad(p[BA  ], x-1, y  , z  )),
                                         FLerp(u,
                                                     grad(p[AB  ], x  , y-1, z  ),
                                                     grad(p[BB  ], x-1, y-1, z  ))),
                          FLerp(v, FLerp(u,
                                                     grad(p[AA+1], x  , y  , z-1),
                                                     grad(p[BA+1], x-1, y  , z-1)),
                                         FLerp(u,
                                                     grad(p[AB+1], x  , y-1, z-1),
                                                     grad(p[BB+1], x-1, y-1, z-1))))
}

func Simplex2(v0 Vector2) float64 {
    // Skew the input space to determine which simplex cell we're in
    s := v0.Sum() * f2 // Hairy factor for 2D
    i := math.Floor(v0.X+s)
    j := math.Floor(v0.Y+s)
    t := (i + j) * g2

    // Unskew the cell origin back to (x,y) space
    v2 := Vector2{i-t, j-t}
    
    // The x,y distances from the cell origin
    v3 := v0.Sub(v2)

    // For the 2D case, the simplex shape is an equilateral triangle.
    // Determine which simplex we are in.
    // Offsets for second (middle) corner of simplex in (i,j) coords
    // lower triangle, XY order: (0,0)->(1,0)->(1,1)
    // upper triangle, YX order: (0,0)->(0,1)->(1,1)
    // A step of (1,0) in (i,j) means a step of (1-c,-c) in (x,y), and
    // a step of (0,1) in (i,j) means a step of (-c,1-c) in (x,y), where
    // c = (3-sqrt(3))/6
    i1 := float64(0.0)
    j1 := float64(0.0)
        
    if (v3.X > v3.Y) { i1 = 1.0 } else { j1 = 1.0 }
    
    // Offsets for middle corner in (x,y) unskewed coords
    v4 := Vector2{v3.X - i1 + g2, v3.Y - j1 + g2}

    // Offsets for last corner in (x,y) unskewed coords
    v5 := Vector2{float64(v3.X - 1.0 + 2.0 * g2), float64(v3.Y - 1.0 + 2.0 * g2)}

    // Work out the hashed gradient indices of the three simplex corners
    ii := int64(i) & 255
    jj := int64(j) & 255

    // Calculate the contribution from the three corners
    v8 := Vector3 {
        0.5 - v3.X*v3.X - v3.Y*v3.Y,
        0.5 - v4.X*v4.X - v4.Y*v4.Y,
        0.5 - v5.X*v5.X - v5.Y*v5.Y,
    }

    // Noise contributions from the 3 corners
    v9 := Vector3{}
    
    if v8.X >= 0 {
        v8.X *= v8.X
        // (x,y) of grad3 used for 2D gradient
        temp := grad3f[p[ii + p[jj]] % 12]
        v9.X = v8.X * v8.X * v3.Dot(Vector2{temp.X, temp.Y})
    }
    
    if v8.Y >= 0 {
        v8.Y *= v8.Y
        temp := grad3f[p[ii + int64(i1) + p[jj + int64(j1)]] % 12]
        v9.Y = v8.Y * v8.Y * v4.Dot(Vector2{temp.X, temp.Y})
    }
    
    if v8.Z >= 0 {
        v8.Z *= v8.Z
        temp := grad3f[p[ii + 1  + p[jj +  1]] % 12]
        v9.Z = v8.Z * v8.Z * v5.Dot(Vector2{temp.X, temp.Y})
    }
    
    return 70.0 * v9.Sum()
}

func Simplex3(v0 Vector3) float64 {
    s  := v0.Sum()*f3 
    i  := math.Floor(v0.X+s)
    j  := math.Floor(v0.Y+s)
    k  := math.Floor(v0.Z+s)
    t  := i+j+k*g3
    
    v1 := Vector3{i-t, j-t, k-t}
    v2 := v0.Sub(v1)
    
    var v3 = Vector3{}
    var v4 = Vector3{}
    
    if v2.X >= v2.Y {
        if v2.Y >= v2.Z {
            v3.X = 1.0
            v4.X = 1.0
            v4.Y = 1.0
        } else if v2.X >= v2.Z {
            v3.X = 1.0
            v4.X = 1.0
            v4.Z = 1.0
        } else {
            v3.Z = 1.0
            v4.X = 1.0
            v4.Z = 1.0
        }
    } else {
        if v2.Y < v2.Z {
            v3.Z = 1.0
            v4.Y = 1.0
            v4.Z = 1.0
        } else if v2.X < v2.Z {
            v3.Y = 1.0
            v4.Y = 1.0
            v4.Z = 1.0
        } else {
            v3.Y = 1.0
            v4.X = 1.0
            v4.Y = 1.0
        }
    }
    
    v5 := v2.Sub(v3).AddF(g3)
    v6 := v2.Sub(v4).AddF(g3*2.0)
    v7 := v2.SubF(1.0).AddF(3.0*g3)
    
    ii := int64(i) & 255
    jj := int64(j) & 255
    kk := int64(k) & 255
    
    // Calculate the contribution from the three corners
    t0 := 0.6 - v2.X*v2.X - v2.Y*v2.Y - v2.Z*v2.Z
    t1 := 0.6 - v5.X*v5.X - v5.Y*v5.Y - v5.Z*v5.Z
    t2 := 0.6 - v6.X*v6.X - v6.Y*v6.Y - v6.Z*v6.Z
    t3 := 0.6 - v7.X*v7.X - v7.Y*v7.Y - v7.Z*v7.Z

    // Noise contributions from the 3 corners
    var r0, r1, r2, r3 float64
    
    if t0 >= 0 {
        t0 *= t0
        // (x,y) of grad3 used for 2D gradient
        gi0 := p[ii +           0 + p[jj +           0 + p[kk +           0]]] % 12
        r0 = t0 * t0 * v3.Dot(grad3f[gi0])
    }
    
    if t1 >= 0 {
        t1 *= t1
        gi1 := p[ii + int64(v3.X) + p[jj + int64(v3.Y) + p[kk + int64(v3.Z)]]] % 12
        r1 = t1 * t1 * v4.Dot(grad3f[gi1])
    }
    
    if t2 >= 0 {
        t2 *= t2
        gi2 := p[ii + int64(v4.X) + p[jj + int64(v4.Y) + p[kk + int64(v4.Z)]]] % 12
        r2 = t2 * t2 * v5.Dot(grad3f[gi2])
    }
    
    if t3 >= 0 {
        t3 *= t3
        gi3 := p[ii +           1 + p[kk +           1 + p[kk +           1]]] % 12
        r3 = t3 * t3 * v6.Dot(grad3f[gi3])
    }
    
    return 32.0 * (r0+r1+r2+r3)
}

func Simplex4(q0 Quaternion) float64 {
    s  := q0.Sum() * f4
    i  := math.Floor(q0.X+s)
    j  := math.Floor(q0.Y+s)
    k  := math.Floor(q0.Z+s)
    l  := math.Floor(q0.W+s)
    t  := i+j+k+l * g4
    
    q1 := Quaternion{i-t, j-t, k-t, l-t}
    q2 := q0.Sub(q1)
    
    var c1, c2, c3, c4, c5, c6 int64
    if q2.X > q2.Y { c1 = 32 }
    if q2.X > q2.Z { c2 = 16 }
    if q2.Y > q2.Z { c3 =  8 }
    if q2.X > q2.W { c4 =  4 }
    if q2.Y > q2.W { c5 =  2 }
    if q2.Z > q2.W { c6 =  1 }
    c := c1 + c2 + c3 + c4 + c5 + c6
    
    simp := simplex[c]
    q3 := Quaternion{}
    if simp[0] >= 3 { q3.X = 1 }
    if simp[1] >= 3 { q3.Y = 1 }
    if simp[2] >= 3 { q3.Z = 1 }
    if simp[3] >= 3 { q3.W = 1 }
    q4 := Quaternion{}
    if simp[0] >= 3 { q4.X = 1 }
    if simp[1] >= 3 { q4.Y = 1 }
    if simp[2] >= 3 { q4.Z = 1 }
    if simp[3] >= 3 { q4.W = 1 }
    q5 := Quaternion{}
    if simp[0] >= 3 { q5.X = 1 }
    if simp[1] >= 3 { q5.Y = 1 }
    if simp[2] >= 3 { q5.Z = 1 }
    if simp[3] >= 3 { q5.W = 1 }
    
    q6 := q2.Sub(q3).AddF(g4)
    q7 := q2.Sub(q4).AddF(2.0*g4)
    q8 := q2.Sub(q5).AddF(3.0*g4)
    q9 := q2.SubF(1.0).AddF(4.0*g4)

    ii := int64(i) & 255
    jj := int64(j) & 255
    kk := int64(k) & 255
    ll := int64(l) & 255
    
    // Calculate the contribution from the three corners
    t0 := 0.6 - q2.X*q2.X - q2.Y*q2.Y - q2.Z*q2.Z - q2.W*q2.W
    t1 := 0.6 - q6.X*q6.X - q6.Y*q6.Y - q6.Z*q6.Z - q6.W*q6.W
    t2 := 0.6 - q7.X*q7.X - q7.Y*q7.Y - q7.Z*q7.Z - q7.W*q7.W
    t3 := 0.6 - q8.X*q8.X - q8.Y*q8.Y - q8.Z*q8.Z - q8.W*q8.W
    t4 := 0.6 - q9.X*q9.X - q9.Y*q9.Y - q9.Z*q9.Z - q9.W*q9.W

    // Noise contributions from the 3 corners
    var r0, r1, r2, r3, r4 float64
    
    if t0 >= 0 {
        t0 *= t0
        // (x,y) of grad3 used for 2D gradient
        gi0 := p[ii +           0 + p[jj +           0 + p[kk +           0 + p[ll     ]]]] % 32
        r0 = t0 * t0 * q2.Dot(grad4f[gi0])
    }
    
    if t1 >= 0 {
        t1 *= t1
        gi1 := p[ii + int64(q3.X) + p[jj + int64(q3.Y) + p[kk + int64(q3.Z) + p[ll + int64(q3.W)]]]] % 32
        r1 = t1 * t1 * q6.Dot(grad4f[gi1])
    }
    
    if t2 >= 0 {
        t2 *= t2
        gi2 := p[ii + int64(q4.X) + p[jj + int64(q4.Y) + p[kk + int64(q4.Z) + p[ll + int64(q4.W)]]]] % 32
        r2 = t2 * t2 * q7.Dot(grad4f[gi2])
    }
    
    if t3 >= 0 {
        t3 *= t3
        gi3 := p[ii + int64(q5.X) + p[jj + int64(q5.Y) + p[kk + int64(q5.Z) + p[ll + int64(q5.W)]]]] % 32
        r3 = t3 * t3 * q8.Dot(grad4f[gi3])
    }
    
    if t4 >= 0 {
        t4 *= t4
        gi4 := p[ii +           1 + p[kk +           1 + p[kk +           1 + p[ll +  1]]]] % 32
        r4 = t4 * t4 * q9.Dot(grad4f[gi4])
    }
    
    return 27.0 * (r0+r1+r2+r3+r4)
}
