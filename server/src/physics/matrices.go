package physics

import (
    "math"
)

// 4x4 matrix
type Matrix struct {
    I11, I12, I13, I14,
    I21, I22, I23, I24,
    I31, I32, I33, I34,
    I41, I42, I43, I44 float64
}

func MatrixFrom3v3(v1, v2, v3 Vector3) Matrix {
    return Matrix{ v1.X, v1.Y, v1.Z, 0.0,
                 v2.X, v2.Y, v2.Z, 0.0,
                 v3.X, v3.Y, v3.Z, 0.0,
                  0.0,  0.0,  0.0, 1.0}
}

func (m *Matrix) ToZero() {
    m.I11 = 0.0
    m.I12 = 0.0
    m.I13 = 0.0
    m.I14 = 0.0
    m.I21 = 0.0
    m.I22 = 0.0
    m.I23 = 0.0
    m.I24 = 0.0
    m.I31 = 0.0
    m.I32 = 0.0
    m.I33 = 0.0
    m.I34 = 0.0
    m.I41 = 0.0
    m.I42 = 0.0
    m.I43 = 0.0
    m.I44 = 0.0
}

func (m *Matrix) ToIdentity() {
    m.I11 = 1.0
    m.I12 = 0.0
    m.I13 = 0.0
    m.I14 = 0.0
    m.I21 = 0.0
    m.I22 = 1.0
    m.I23 = 0.0
    m.I24 = 0.0
    m.I31 = 0.0
    m.I32 = 0.0
    m.I33 = 1.0
    m.I34 = 0.0
    m.I41 = 0.0
    m.I42 = 0.0
    m.I43 = 0.0
    m.I44 = 1.0
}

func (m *Matrix) ToTranslation(v Vector3) {
    m.I11 = 1.0
    m.I12 = 0.0
    m.I13 = 0.0
    m.I14 = v.X
    m.I21 = 0.0
    m.I22 = 1.0
    m.I23 = 0.0
    m.I24 = v.Y
    m.I31 = 0.0
    m.I32 = 0.0
    m.I33 = 1.0
    m.I34 = v.Z
    m.I41 = 0.0
    m.I42 = 0.0
    m.I43 = 0.0
    m.I44 = 1.0
}

func (m *Matrix) ToScale(s float64) {
    m.I11 = s
    m.I12 = 0.0
    m.I13 = 0.0
    m.I14 = 0.0
    m.I21 = 0.0
    m.I22 = s
    m.I23 = 0.0
    m.I24 = 0.0
    m.I31 = 0.0
    m.I32 = 0.0
    m.I33 = s
    m.I34 = 0.0
    m.I41 = 0.0
    m.I42 = 0.0
    m.I43 = 0.0
    m.I44 = 1.0    
}

func (m *Matrix) ToDiagonalFromV3(v Vector3) {
    m.I11 = v.X
    m.I12 = 0.0
    m.I13 = 0.0
    m.I14 = 0.0
    m.I21 = 0.0
    m.I22 = v.Y
    m.I23 = 0.0
    m.I24 = 0.0
    m.I31 = 0.0
    m.I32 = 0.0
    m.I33 = v.Z
    m.I34 = 0.0
    m.I41 = 0.0
    m.I42 = 0.0
    m.I43 = 0.0
    m.I44 = 1.0    
}

func (m *Matrix) ToDiagonalFromQ(q Quaternion) {
    m.I11 = q.X
    m.I12 = 0.0
    m.I13 = 0.0
    m.I14 = 0.0
    m.I21 = 0.0
    m.I22 = q.Y
    m.I23 = 0.0
    m.I24 = 0.0
    m.I31 = 0.0
    m.I32 = 0.0
    m.I33 = q.Z
    m.I34 = 0.0
    m.I41 = 0.0
    m.I42 = 0.0
    m.I43 = 0.0
    m.I44 = q.W
}

func (m *Matrix) ToRotation(angle float64, axis Vector3) {
    if axis.LenSquared() < epsilonSquared {
        m.ToIdentity()
    } else {
        axis.INorm()
        var (
            Cos float64 = math.Cos(angle)
            OneMinusCos float64 = 1.0-Cos
            V1 = axis.Mul(axis)
            V2 = V1.MulF(OneMinusCos)
            V3 = axis.MulF(math.Sin(angle))
        )
        m.I11 = V1.X * OneMinusCos + Cos
        m.I12 = V2.X - V3.Z
        m.I13 = V2.Y + V3.Y
        m.I14 = 0.0
        
        m.I21 = V2.X + V3.Z
        m.I22 = V1.Y * OneMinusCos + Cos
        m.I23 = V2.Z - V3.X
        m.I24 = 0.0
    
        m.I31 = V2.Y - V3.Y
        m.I32 = V2.Z + V3.X
        m.I33 = V1.Z * OneMinusCos + Cos
        m.I34 = 0.0
        
        m.I41 = 0.0
        m.I42 = 0.0
        m.I43 = 0.0
        m.I44 = 1.0
    }
}

func (m *Matrix) LookAt(eye, at, up Vector3) {
    z_axis := at.Sub(eye)
    z_axis.INorm()
    x_axis := up.Cross(z_axis)
    x_axis.INorm()
    y_axis := z_axis.Cross(x_axis)
    y_axis.INorm()
    
    m.I11 = x_axis.X
    m.I12 = x_axis.Y
    m.I13 = x_axis.Z
    m.I14 = -x_axis.Dot(eye)
    
    m.I21 = y_axis.X
    m.I22 = y_axis.Y
    m.I23 = y_axis.Z
    m.I24 = -y_axis.Dot(eye)
    
    m.I31 = z_axis.X
    m.I32 = z_axis.Y
    m.I33 = z_axis.Z
    m.I34 = -z_axis.Dot(eye)
    
    m.I41 = 0.0
    m.I42 = 0.0
    m.I43 = 0.0
    m.I44 = 1.0
}




