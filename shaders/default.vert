#version 330

uniform Transformation {
    mat4 proj_matrix;
    mat4 modv_matrix;
};

in vec3 vertex;

void main(void) {
    gl_Position = proj_matrix*modv_matrix*vec4(vertex, 1.0);
}
