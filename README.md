Oh look
=======

A penny!

License
=======

Check LICENSE for the license covering all work except that listed below in credits.
It's the MIT License.


Credits
=======

Temporary art assets: http://www.reinerstilesets.de/
Shader awesomeness:   http://code.google.com/p/layer/source/browse/layer/layer/shader.py

