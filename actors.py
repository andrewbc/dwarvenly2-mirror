#!/usr/bin/env python
# encoding: utf-8
"""
actors.py

Created by Andrew on 2011-08-03.
Copyright (c) 2011 __MyCompanyName__. All rights reserved.
"""

class Actor:
    def __init__(self, x=0.0, y=0.0, z=0.0, x_vel=0.0, y_vel=0.0, z_vel=0.0):
        self.x = self.x_ = x
        self.y = self.y_ = y
        self.z = self.z_ = z
        self.x_vel = x_vel
        self.y_vel = y_vel
        self.z_vel = z_vel
        
class Character(Actor):
    str = 0.0
    dex = 0.0
    con = 0.0
    int = 0.0
    wis = 0.0
    cha = 0.0
    luk = 0.0
    age = 0
    height = 0.0
    weight = 0.0
    speed = 0.0
    morality = "Neutral"
    discipline = "Neutral"
    race=None
    profession=None

    def __init__(self,
                 str=0.0, dex=0.0, con=0.0, int=0.0, wis=0.0, cha=0.0, luk=0.0,
                 morality=None, discipline=None, age=0, height=0.0, weight=0.0, speed=3.0,
                 race=None, profession=None, is_alive=True,
                 x_vel=0.0, y_vel=0.0, z_vel=0.0, x=0.0, y=0.0, z=0.0, group=None):
        Actor.__init__(self, x=x, y=y, z=z, x_vel=x_vel, y_vel=y_vel, z_val=z_val)
        self.str += str
        self.dex += dex
        self.con += con
        self.int += int
        self.wis += wis
        self.cha += cha
        self.luk += luk
        self.age += age
        self.height += height
        self.weight += weight
        self.group = group

        if morality is not None:
            self.morality = morality # good vs. evil
        if discipline is not None:
            self.discipline = discipline # lawful vs. chaotic
        if race is not None:
            self.race = race
        if profession is not None:
            self.profession = profession
        self.is_alive = is_alive
        self.enemy = None
        self.deadliness = 0.0
        self.speed = speed
        self.movable = False
        self._movement_lock = 1 / self.speed

    def on_movement_possible(self):
        self.movable = True
        self._movement_lock = 0.0

    def on_movement_impossible(self):
        self.movable = False        
        self._movement_lock = 1/self.speed

    def update(self, dt=0.0):
        """Update our physical features."""
        if self.movable:
            if self.rel_x != 0 or self.rel_y != 0:
                self.dispatch_event('on_movement')
                # Make sure we lock movement again.
                self.dispatch_event('on_movement_impossible')
        else:
            self._movement_lock -= dt
            if self._movement_lock <= 0.0:
                self.dispatch_event('on_movement_possible')


    def on_die(self, dt=0.0):
        self.is_alive = False

class Item(Actor):
    def __init__(self, x=0.0, y=0.0, z=0.0, group=None):
        Actor.__init__(self, x=x, y=y, z=z)
        self.group = group
