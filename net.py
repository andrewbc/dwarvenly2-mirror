#!/usr/bin/env python
import socket
import json
import time
import collections
import pyglet

class Net(object):
    def __init__(self, rhost, rport):
        print("initializing net loop")
        self.map = {}
        self.outs = collections.deque()
        sock = None
        timeout = 1
        while sock is None:
            print("Setting up connection to: %s:%s"%(rhost, rport))
            try:
                sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                sock.bind(("", 44044))
                sock.setblocking(False)
            except socket.error as (errno, msg):
                print("sock creation error: ", msg)
    
        def net_loop(update):
            # Handle sending messages to socket.
            while True:
                try:
                    out = self.outs.popleft()
                except IndexError:
                    break
                print('sending: %s' % (repr(out)))
                num_sent = sock.sendto(out, (rhost, rport))
                # Making sure we actually sent everything.
                while num_sent < len(out):
                    print("num_sent: %s, out_buffer: '%s'" % (str(num_sent), out))
                    out = out[num_sent:]
                    num_sent = sock.sendto(out, (rhost, rport))
    
            ins = ""
            # Handle getting messages from socket.
            try:
                s, raddr = sock.recvfrom(4096)
                if raddr == (rhost, rport):
                    ins += s
            except socket.error as (errno, msg):
                if errno == 11:
                    pass
    
            # Handle sending messages to parent.
            for msg in ins.split('\0'):
                if len(msg) > 0:
                    print(msg)
                    jmsg = json.loads(msg)
                    print(repr(jmsg))
                    if 'uid' in jmsg:
                        uid = jmsg['uid']
                        if uid in self.map:
                            response = self.map[uid]
                            if callable(response):
                                response(jmsg)
                            elif isinstance(response, collections.Iterable):
                                for callback in response:
                                    callback(jmsg)
        pyglet.clock.schedule_interval(net_loop, 1/60.0)

    def send(self, type="net", **kwargs):
        uid = time.time()
        kwargs.update(type=type, uid=uid)
        self.map[uid] = kwargs["re"]
        del kwargs["re"]
        msg = json.dumps(kwargs, separators=(',',':'))
        print("!!! "+repr(msg))
        self.outs.append(msg)