#!/usr/bin/env python
from distutils.core import setup, Extension

setup(name="engine",
      version="0.0.1",
      description="Engine for Dwarvenly",
      author="AndrewBC",
      author_email="andrew.b.coleman@gmail.com",
      url="http://andrewbc.net/",
      ext_modules=[
        Extension("engine",
                  libraries=['glfw'],
				  library_dirs=['/usr/local/lib'],
                  sources=["engine/gl3w.c", "engine/enginemodule.c"],
				  include_dirs=['/usr/local/include'],
                  extra_compile_args=["-Wall", "-Wextra", "-std=c99"]
                  ),
      ])
